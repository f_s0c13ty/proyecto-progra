package utilities;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.*;

/**
 * This class contains static methods to handle exportation processes
 */
public class ExportationHandler {

    /**
     * Exports part of a canvas as a png image. The area of it to be saved is
     * counted from the top-left corner and extended "width" pixels to the right
     * and "height" pixels down.
     *
     * @param canvas Canvas object to be exported
     * @param width  width of the section of the canvas to be exported
     * @param height height of the section of the canvas to be exported
     */
    public static void exportAsPNG(Canvas canvas, int width, int height) {
        Stage stage = (Stage) canvas.getScene().getWindow();
        WritableImage writableImage = getCanvasWritableImage(canvas, width, height);
        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
        // Set and show file chooser
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Image");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Portable Network Graphics (.png)", "*.png"));
        File file = fileChooser.showSaveDialog(stage);
        // Export image
        try {
            ImageIO.write(renderedImage, "png", file);
        } catch (IOException e) {
            // Ignore this exception
            // e.printStackTrace();
        }
    }

    /**
     * Exports part of a canvas as PDF. To do so, a PDF document and and image
     * of the canvas are created. The latter is inserted into te first.
     * The area of the canvas to be saved is counted from the top-left corner
     * and extended "width" pixels to the right and "height" pixels down.
     *
     * @param canvas Canvas object to be exported
     * @param width  width of the section of the canvas to be exported
     * @param height height of the section of the canvas to be exported
     */
    public static void exportAsPDF(Canvas canvas, int width, int height) {
        Stage stage = (Stage) canvas.getScene().getWindow();
        // show file chooser to select the name and directory of the new document
        Document document = createDocument(stage);
        // get image of the canvas
        WritableImage writableImage = getCanvasWritableImage(canvas, width, height);
        // convert the image to a format IText can write into the pdf
        Image image = convertToItextImage(writableImage);
        // insert the image in the PDF
        insertImageIntoPDF(document, image);
    }


    /**
     * Takes a snapshot of the canvas and returns it as a WritableImage object
     *
     * @param canvas Canvas object
     * @param width  width of the section of the canvas to be exported
     * @param height height of the section of the canvas to be exported
     * @return       WritableImage object containing a snapshot of the canvas
     */
    private static WritableImage getCanvasWritableImage(Canvas canvas, int width, int height) {
        Stage stage = (Stage) canvas.getScene().getWindow();
        // Set area of the canvas to be exported
        WritableImage writableImage = new WritableImage(width + 20, height + 20);
        // Take snapshot of the selected area
        canvas.snapshot(null, writableImage);
        return writableImage;
    }

    /**
     * Converts a WritableImage object into a com.itextpdf.text.Image object.
     * It is used later to insert an image into a PDF
     *
     * @param writableImage WritableImage object
     * @return              com.itextpdf.text.Image object
     */
    private static Image convertToItextImage(WritableImage writableImage) {
        Image  image = null;
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        try {
            ImageIO.write( SwingFXUtils.fromFXImage(writableImage, null ), "png", byteOutput);
            image = Image.getInstance(byteOutput.toByteArray());
        } catch (BadElementException | IOException e) {
            // Ignore this exception
            // e.printStackTrace();
        }
        return image;
    }

    /**
     * Inserts an com.itextpdf.text.Image into a PDF
     *
     * @param document PDF where to put the image
     * @param image    com.itextpdf.text.Image to insert into the PDF
     */

    private static void insertImageIntoPDF(Document document, Image image) {
        document.open();
        float documentWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
        float documentHeight = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();

        image.scaleToFit(documentWidth, documentHeight);
        try {
            document.add(image);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();
    }

    /**
     * Creates an empty PDF in the specified directory.
     *
     * @param stage  the Stage object where to show the file chooser
     * @return       the new com.itextpdf.text Document
     */
    private static Document createDocument(Stage stage) {
        Document document = new Document();
        // Set and show file chooser
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export as PDF");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Portable Document Format (.pdf)", "*.pdf"));
        File file = fileChooser.showSaveDialog(stage);
        String fileName = file.toString();

        // Save the PDF in the new directory
        try {
            PdfWriter.getInstance(document, new FileOutputStream(fileName));
        } catch (DocumentException | FileNotFoundException e) {};
        return document;
    }


}
