package utilities;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class has static methods used for loading views
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 0X.09.18 (Modified by: xxxx)
 */
public class SceneHandler {
    /**
     * Loads a new scene
     *
     * @param viewRoot Pane whence to get the Stage
     * @param resource URL of the fxml file to be loaded
     */
    static public void loadView(Pane viewRoot, URL resource){
        Stage stage = (Stage) viewRoot.getScene().getWindow();
        Parent root = null;
        try {
            root = FXMLLoader.load(resource);
        } catch (IOException ex) {
            System.out.println("Couldn't load the view" + resource.toString());
            Logger.getLogger(SceneHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
    }

    /**
     * Loads a pop-up window, keeping the previous one open
     * @param resource      URL of the fxml file to be loaded
     * @param topLeftCorner Coordinate of the top left corner where to place the pop up window
     */
    public static void loadPopUp(URL resource, Point2D topLeftCorner){
        Parent root;
        try {
            root = FXMLLoader.load(resource);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(SceneHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Loads a pop-up window, keeping the previous one open, setting its name
     * @param title         Title of the window
     * @param resource      URL of the fxml file to be loaded
     * @param topLeftCorner Coordinate of the top left corner where to place the pop up window
     */
    public static void loadPopUp(String title, URL resource, CustomPoint2D topLeftCorner){
        Parent root;
        try {
            root = FXMLLoader.load(resource);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(SceneHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
