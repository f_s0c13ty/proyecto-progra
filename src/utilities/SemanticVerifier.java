package utilities;

import application.Main;
import model.Attribute;
import model.AttributeType;
import model.Entity;
import model.Relation;

import java.util.ArrayList;

/**
 * <code>SemanticVerifier</code>
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 14.12.18 (Modified by: xxxx)
 */

public class SemanticVerifier {

    public static StringBuilder warningContent;

    /**
     * Verifies that there are not name conflicts among attributes.
     * Name conflicts occur when a child entity has an attribute with the same
     * label as an attribute of its parent.
     */
    public static void attributeChildNameAvailable() {
        int index;
        ArrayList<String> names = new ArrayList<>();
        for (Attribute attribute : Main.diagram.getAttributes()) {
            if (names.contains(attribute.getName())) {
                index = names.indexOf(attribute.getName());
                //Given that a duplicated name was found, need to check if there
                // are any name conflicts.
                if (thereAreNameConflictsBetweenAttributes(attribute, Main.diagram.getAttributes().get(index))) {
                    warningContent.append("- Attribute named '" + attribute.getName() + "' needs to be renamed\n");
                }
            }
            names.add(attribute.getName());
        }
    }

    /**
     * entityNameHasBeenTaken: Method that checks if the name of an entity has already been taken for another entity
     *
     * @param name
     * @return true if an entity already has that name
     */
    public static boolean entityNameHasBeenTaken(String name) {
        for (Entity entity : Main.diagram.getEntities()) {
            if (entity.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifies if there are name conflicts between 2 attributes that have the
     * same label. Name conflicts occur when a child entity has an attribute
     * with the same label as an attribute of its parent.
     *
     * @param a1 Attribute 1
     * @param a2 Attribute 2
     * @return True if there are name conflicts; false otherwise
     */
    private static boolean thereAreNameConflictsBetweenAttributes(Attribute a1, Attribute a2) {
        boolean thereAreConflicts = false;
        if(a1.getCompositeAttribute() != null && a2.getCompositeAttribute() == null) {

            if (a1.getCompositeAttribute().getEntityConnected() != null && a1.getCompositeAttribute().getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a1.getCompositeAttribute(), a2)) {
                    thereAreConflicts = true;
                }
            } else if (a2.getEntityConnected() != null && a2.getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a2, a1.getCompositeAttribute())) {
                    thereAreConflicts = true;
                }
            }

        }else if(a2.getCompositeAttribute() != null && a1.getCompositeAttribute() == null){

            if (a1.getEntityConnected() != null && a1.getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a1, a2.getCompositeAttribute())) {
                    thereAreConflicts = true;
                }
            } else if (a2.getCompositeAttribute().getEntityConnected() != null && a2.getCompositeAttribute().getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a2.getCompositeAttribute(), a1)) {
                    thereAreConflicts = true;
                }
            }

        }else if(a1.getCompositeAttribute() != null && a2.getCompositeAttribute() != null) {

            if (a1.getCompositeAttribute().getEntityConnected() != null && a1.getCompositeAttribute().getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a1.getCompositeAttribute(), a2.getCompositeAttribute())) {
                    thereAreConflicts = true;
                }
            }else if (a2.getCompositeAttribute().getEntityConnected() != null && a2.getCompositeAttribute().getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a2.getCompositeAttribute(), a1.getCompositeAttribute())) {
                    thereAreConflicts = true;
                }
            }

        }else {
            if (a1.getEntityConnected() != null && a1.getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a1, a2)) {
                    thereAreConflicts = true;
                }
            } else if (a2.getEntityConnected() != null && a2.getEntityConnected().getInheritance() != null) {
                if (firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(a2, a1)) {
                    thereAreConflicts = true;
                }
            }
        }
        return thereAreConflicts;
    }

    private static boolean firstAttributeIsConnectedToAnEntityThatIsParentOfTheEntityOfSecondAttribute(Attribute a1, Attribute a2) {
        return a1.getEntityConnected().getInheritance().getChildren().contains(a2.getEntityConnected());
    }

    /**
     * Checks that every weak entity has a partial key attribute associated. It
     * displays a message when this is not the case.
     */
    public static void verifyEveryWeakEntityHasPartialKey() {
        boolean entityHasPartialKey;
        for (Entity entity : Main.diagram.getEntities()) {
            entityHasPartialKey = false;
            if (entity.isWeak()) {
                if (relationConnected(entity)) {
                    if (Main.diagram.getAttributes().isEmpty()) {
                        warningContent.append("- Weak entities should have a partial key attribute\n");
                        warningContent.append("- Strong entities connected to weak entities should have key attributes\n");
                    } else {
                        for (Attribute attribute : Main.diagram.getAttributes()) {
                            if (attribute.getEntityConnected() != null && attribute.getEntityConnected().equals(entity)) {
                                if (attribute.connectedEntityIsWeakAndHasPartialKey()) {
                                    entityHasPartialKey = true;
                                    break;
                                }
                            }

                        }
                        if (!entityHasPartialKey) {
                            warningContent.append("- " + entity.getName() + " should have a partial key\n");
                        }
                    }
                } else {
                    warningContent.append("- " + entity.getName() + " can't be weak by itself\n");
                }
            }
        }
    }

    private static boolean relationConnected(Entity entity) {
        for (Relation relation : Main.diagram.getRelations()) {
            if (relation.getEntities().containsValue(entity))
                return true;
        }
        return false;
    }

    /**
     * Checks that every weak entity connected to a relation does it in a proper
     * way
     */
    public static void verifyWeakEntitiesRelation() {
        Main.diagram.getRelations().forEach(relation -> {
            if (relation.getEntities().size() == 2) {
                if (relation.getEntity(0).isWeak()) {
                    if (!relation.isWeak()) {
                        warningContent.append("- " + relation.getName() + " must be weak\n");
                    }
                    if (!relation.getEntity(1).isWeak()) {
                        for (Attribute attribute : Main.diagram.getAttributes()) {
                            if (attribute.getAttributeType().equals(AttributeType.KEY)
                                    && attribute.getEntityConnected().equals(relation.getEntity(1))) {
                                return;
                            }
                        }
                        warningContent.append("- " + relation.getEntity(1).getName() + " needs a key attribute\n");
                    } else {
                        warningContent.append("- " + relation.getName() + " can't have both entities weak\n");
                    }
                } else if (relation.getEntity(1).isWeak()) {
                    if (!relation.isWeak()) {
                        warningContent.append("- " + relation.getName() + " must be weak\n");
                    }
                    if (!relation.getEntity(0).isWeak()) {
                        for (Attribute attribute : Main.diagram.getAttributes()) {
                            if (attribute.getAttributeType().equals(AttributeType.KEY) && attribute.getEntityConnected().equals(relation.getEntity(0))) {
                                return;
                            }
                        }
                        warningContent.append("- " + relation.getEntity(0).getName() + " needs a key attribute\n");
                    } else {
                        warningContent.append("- " + relation.getName() + " can't have both entities weak\n");
                    }
                }
            }
            // TODO: Maybe it's necessary implement the same for two aggregations or an entity and an aggregation, we should ask
        });
    }

    public static void verifyRelationsAndAttributesNotConnected(){
        Main.diagram.getAttributes().forEach(attribute -> {
            if(attribute.getCompositeAttribute() == null && attribute.getEntityConnected()  == null && attribute.getRelationConnected() == null)
                warningContent.append("- " + attribute.getName() + " can't exist by itself\n");
        });

        Main.diagram.getRelations().forEach(relation -> {
            if(relation.getEntities().isEmpty() && relation.getAggregations().isEmpty())
                warningContent.append("- " + relation.getName() + " can't exist without a connection to either an Entity or an Aggregation\n");
        });
    }
}
