package utilities;

public class CustomPoint2D implements Cloneable {

    private double x;
    private double y;

    /**
     * Creates a new instance of {@code Point2D}.
     *
     * @param x the x coordinate of the point
     * @param y the y coordinate of the point
     */
    public CustomPoint2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public CustomPoint2D clone(){
        CustomPoint2D obj = null;
        try{
            obj = (CustomPoint2D) super.clone();
        }catch (CloneNotSupportedException ex){
            System.out.println("Can't clone CustomPoint2D");
        }
        return obj;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distance(double x1, double y1) {
        double a = getX() - x1;
        double b = getY() - y1;
        return Math.sqrt(a * a + b * b);
    }

    public double distance(CustomPoint2D point) {
        return distance(point.getX(), point.getY());
    }

    public CustomPoint2D midpoint(double x, double y) {
        return new CustomPoint2D(
                x + (getX() - x) / 2.0,
                y + (getY() - y) / 2.0);
    }

    public CustomPoint2D midpoint(CustomPoint2D point) {
        return midpoint(point.getX(), point.getY());
    }

    public double angle(double x, double y) {
        final double ax = getX();
        final double ay = getY();

        final double delta = (ax * x + ay * y) / Math.sqrt(
                (ax * ax + ay * ay) * (x * x + y * y));

        if (delta > 1.0) {
            return 0.0;
        }
        if (delta < -1.0) {
            return 180.0;
        }

        return Math.toDegrees(Math.acos(delta));
    }

    public double angle(CustomPoint2D point) {
        return angle(point.getX(), point.getY());
    }

    public double angle(CustomPoint2D p1, CustomPoint2D p2) {
        final double x = getX();
        final double y = getY();

        final double ax = p1.getX() - x;
        final double ay = p1.getY() - y;
        final double bx = p2.getX() - x;
        final double by = p2.getY() - y;

        final double delta = (ax * bx + ay * by) / Math.sqrt(
                (ax * ax + ay * ay) * (bx * bx + by * by));

        if (delta > 1.0) {
            return 0.0;
        }
        if (delta < -1.0) {
            return 180.0;
        }

        return Math.toDegrees(Math.acos(delta));
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof CustomPoint2D) {
            CustomPoint2D other = (CustomPoint2D) obj;
            return getX() == other.getX() && getY() == other.getY();
        } else return false;
    }

    public CustomPoint2D subtract(double x, double y) {
        return new CustomPoint2D(
                getX() - x,
                getY() - y);
    }

    public CustomPoint2D subtract(CustomPoint2D point) {
        return subtract(point.getX(), point.getY());
    }
}
