/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Diagram;

import java.io.IOException;
import java.util.Stack;

/**
 * This class contains the main method to start the application
 *
 * @author  Edward Becerra
 * @author  Carlos Rios
 * @author  Jose Villar
 * @version 09.09.18 (Modified by: Jose Villar)
 */
public class Main extends Application {

    public static Diagram diagram;
    public static Stack<Diagram> stackUndo = new Stack<>(), stackRedo = new Stack<>();

    @Override
    public void start(Stage primaryStage) {

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/MainWindow.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.setResizable(true);
        primaryStage.setTitle("Entity-Relation Diagrams Maker");
        primaryStage.show();
        primaryStage.toFront();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        diagram = new Diagram();
        launch(args);
    }

    public static int saveDiagramState(){
        stackRedo.clear();
        stackUndo.add(diagram.clone());
        if(stackUndo.size() >= 10)
            stackUndo.remove(0);
        return stackUndo.size();
    }

    public static int undo(){
        if(!stackUndo.empty()){
            stackRedo.add(diagram);
            diagram = stackUndo.pop();
        }
        return stackUndo.size();
    }

    public static int redo(){
        if(!stackRedo.empty()){
            stackUndo.add(diagram);
            diagram = stackRedo.pop();
        }
        return stackUndo.size();
    }
}
