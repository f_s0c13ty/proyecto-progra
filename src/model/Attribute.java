package model;

import application.Main;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.figures.Ellipse;
import model.figures.Polygon;
import utilities.DateHelper;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 * Attribute class: Attribute of an Entity. Contain the name and types of the attribute
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 21.10.18 (Modified by: Edward Becerra)
 */
public class Attribute implements Cloneable{

    private static int nameID = 1;

    private long id;

    // Name used to identify the attribute
    private String name;

    // Type of the attribute (PrimaryKey, ForeignKey, Calculated, Derivative)
    private AttributeType attributeType;

    private Polygon ellipse;

    private ArrayList<Attribute> attributes = new ArrayList<>();

    private Attribute CompositeAttribute;

    private Entity entityConnected;

    private Relation relationConnected;

    public Attribute(String name, AttributeType attributeType) {
        if (name.equals(""))
            this.name = "A" + nameID++;
        else
            this.name = name;
        this.id = DateHelper.getUTCTicks(Date.from(Instant.now()));
        this.attributeType = attributeType;
        this.entityConnected = null;
        this.relationConnected = null;
    }

    public Attribute(){}

    public long getId() {
        return id;
    }

    public static int getNameID() {
        return nameID;
    }

    public static void resetID() {
        nameID = 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    public void addAttributes(Attribute attribute){
        if(this.attributeType.equals(AttributeType.COMPOSITE)){
            this.attributes.add(attribute);
        }
    }

    public void setId(long id) {
        this.id = id;
    }

    public void removeAttribute(Attribute attribute){
        this.attributes.remove(attribute);
    }

    public Entity getEntityConnected() {
        return entityConnected;
    }

    public void setEntityConnected(Entity entityConnected) {
        this.entityConnected = entityConnected;
    }

    public Relation getRelationConnected() {
        return relationConnected;
    }

    public void setRelationConnected(Relation relationConnected) {
        this.relationConnected = relationConnected;
    }

    public Polygon getEllipse() {
        return ellipse;
    }

    public Attribute getCompositeAttribute() {
        return CompositeAttribute;
    }

    public void setCompositeAttribute(Attribute compositeAttribute) {
        CompositeAttribute = compositeAttribute;
    }

    public void setMyFigure(double centerX, double centerY) {
        this.ellipse = new Ellipse(this.name, centerX,centerY);
    }

    public void showOnCanvas(GraphicsContext gc, Color color) {
        ellipse.setAvailablePoints();
        switch (this.attributeType) {
            case GENERIC:
                ellipse.draw(gc, color);
                break;
            case KEY:
                ellipse.setUnderlinedLabel(true);
                ellipse.draw(gc, color);
                break;
            case MULTIVALUED:
                ellipse.drawDoubleLined(gc, color);
                break;
            case COMPOSITE:
                ellipse.draw(gc, color);
                break;
            case DERIVATIVE:
                ellipse.drawDashed(gc, color);
                break;
            case PARTIAL_KEY:
                ellipse.setUnderlinedLabelDashed(true);
                ellipse.draw(gc, color);
            default:
                break;
        }
        if(relationConnected != null){
            ConnectingLine.draw(gc,this,relationConnected);
        }else if(entityConnected != null){
            ConnectingLine.draw(gc,this,entityConnected);
        }
        attributes.forEach(attribute -> {
            attribute.getEllipse().setAvailablePoints();
            ConnectingLine.draw(gc,this, attribute);
        });
    }

    /**
     * If the entity connected to this attribute is weak, it should have at
     * least one partial key attribute. This method is used to verify if this
     * attribute is the partial key needed by that entity.
     *
     * @return true if this attribute is a partial key and the connected entity
     *         is weak. False otherwise
     */
    public boolean connectedEntityIsWeakAndHasPartialKey (){
        boolean connectedEntityIsWeakAndHasPartialKey = false;
        if(this.getEntityConnected() != null) {
            if(this.getEntityConnected().isWeak()) {
                if(this.attributeType.equals(AttributeType.PARTIAL_KEY)) {
                    connectedEntityIsWeakAndHasPartialKey = true;
                }
            }
        }
        return connectedEntityIsWeakAndHasPartialKey;

    }


    public void setClonedAttributes(ArrayList<Attribute> attributesCloned){
        ArrayList<Attribute> aux = new ArrayList<>();
        attributes.forEach(att ->
        {
            for (Attribute cloned : attributesCloned) {
                if (att.getId() == cloned.getId()) {
                    aux.add(cloned);
                    break;
                }
            }
        });
        this.attributes = aux;
    }

    public Attribute clone(ArrayList<Entity> entitiesCloned, ArrayList<Relation> relationCloned){
        Attribute obj = new Attribute();
        obj.setId(this.id);
        obj.setName(this.name);
        obj.setAttributeType(this.attributeType);
        obj.ellipse = ellipse.clone();
        obj.setAttributes(attributes);

        if(entityConnected != null) {
            for (Entity entity : entitiesCloned) {
                if (entityConnected.getId() == entity.getId()){
                    obj.setEntityConnected(entity);
                    break;
                }
            }
        }

        if(relationConnected != null) {
            for (Relation relation : relationCloned) {
                if (relationConnected.getId() == relation.getId()){
                    obj.setRelationConnected(relation);
                    break;
                }
            }
        }

        return obj;
    }

}
