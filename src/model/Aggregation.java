package model;

import application.Main;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.figures.Polygon;
import model.figures.Rectangle;
import utilities.CustomPoint2D;

import java.util.ArrayList;
import java.util.HashMap;

public class Aggregation {

    private static int nameID = 1;
    private String name;
    private ArrayList<Entity> entities;
    private ArrayList<Attribute> attributes;
    private ArrayList<Relation> relations;
    private ArrayList<Aggregation> aggregations;
    private ArrayList<CustomPoint2D> entitiesCenters;
    private ArrayList<CustomPoint2D> attributesCenters;
    private ArrayList<CustomPoint2D> relationsCenters;
    private Rectangle polygon;

    public Aggregation(String name, Relation relation) {
        if (name.equals(""))
            this.name = "AGGREGATION" + nameID++;
        else
            this.name = name;
        this.entities = new ArrayList<>();
        HashMap<ConnectingLine, Entity> entitiesHashMap = relation.getEntities();
        entitiesHashMap.forEach((k, v) -> this.entities.add(v));
        this.attributes = new ArrayList<>();
        this.relations = new ArrayList<>();
        relations.add(relation);
        for (Attribute attribute : Main.diagram.getAttributes()) {
            if (this.entities.contains(attribute.getEntityConnected()) || this.relations.contains(attribute.getRelationConnected()))
                this.attributes.add(attribute);
            if(attribute.getCompositeAttribute() != null && (this.entities.contains(attribute.getCompositeAttribute().getEntityConnected()) || this.relations.contains(attribute.getCompositeAttribute().getRelationConnected())))
                this.attributes.add(attribute);
        }
        this.aggregations = new ArrayList<>();
        HashMap<ConnectingLine, Aggregation> aggregationHashMap = relation.getAggregations();
        aggregationHashMap.forEach((k, v) -> aggregations.add(v));
        this.entitiesCenters = new ArrayList<>();
        this.attributesCenters = new ArrayList<>();
        this.relationsCenters = new ArrayList<>();
        this.polygon = new Rectangle(this.name, computeTopLeftCorner(), computeBottomRightCorner());
    }

    public static void resetID() {
        nameID = 1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Entity> getEntities() {
        return this.entities;
    }

    public ArrayList<Relation> getRelations() {
        return this.relations;
    }

    public ArrayList<Attribute> getAttributes() {
        return this.attributes;
    }

    public ArrayList<Aggregation> getAggregations() {
        return this.aggregations;
    }

    public void addAttribute(Attribute attribute) {
        this.attributes.add(attribute);
    }

    public Rectangle getPolygon() {
        return this.polygon;
    }

    private CustomPoint2D computeTopLeftCorner() {
        int minX, minY;
        if (!this.entities.isEmpty()) {
            minX = this.entities.get(0).getPolygon().getMinX();
            minY = this.entities.get(0).getPolygon().getMinY();
        } else if (!this.relations.isEmpty()) {
            minX = this.relations.get(0).getPolygon().getMinX();
            minY = this.relations.get(0).getPolygon().getMinY();
        } else {
            minX = this.aggregations.get(0).polygon.getMinX();
            minY = this.aggregations.get(0).polygon.getMinY();
        }
        int minXTemp, minYTemp;
        for (Entity entity : this.entities) {
            minXTemp = entity.getPolygon().getMinX();
            minYTemp = entity.getPolygon().getMinY();
            if (minXTemp < minX)
                minX = minXTemp;
            if (minYTemp < minY)
                minY = minYTemp;
        }
        for (Attribute attribute : this.attributes) {
            minXTemp = attribute.getEllipse().getMinX();
            minYTemp = attribute.getEllipse().getMinY();
            if (minXTemp < minX)
                minX = minXTemp;
            if (minYTemp < minY)
                minY = minYTemp;
        }
        for (Relation relation : this.relations) {
            minXTemp = relation.getPolygon().getMinX();
            minYTemp = relation.getPolygon().getMinY();
            if (minXTemp < minX)
                minX = minXTemp;
            if (minYTemp < minY)
                minY = minYTemp;
        }
        for (Aggregation aggregation : this.aggregations) {
            minXTemp = aggregation.polygon.getMinX();
            minYTemp = aggregation.polygon.getMinY();
            if (minXTemp < minX)
                minX = minXTemp;
            if (minYTemp < minY)
                minY = minYTemp;
        }
        return new CustomPoint2D(minX - 15 * Polygon.zoom, minY - 45 * Polygon.zoom);
    }

    private CustomPoint2D computeBottomRightCorner() {
        int maxX, maxY;
        if (!this.entities.isEmpty()) {
            maxX = this.entities.get(0).getPolygon().getMaxX();
            maxY = this.entities.get(0).getPolygon().getMaxY();
        } else if (!this.relations.isEmpty()) {
            maxX = this.relations.get(0).getPolygon().getMaxX();
            maxY = this.relations.get(0).getPolygon().getMaxY();
        } else {
            maxX = this.aggregations.get(0).polygon.getMaxX();
            maxY = this.aggregations.get(0).polygon.getMaxY();
        }
        int maxXTemp, maxYTemp;
        for (Entity entity : this.entities) {
            maxXTemp = entity.getPolygon().getMaxX();
            maxYTemp = entity.getPolygon().getMaxY();
            if (maxXTemp > maxX)
                maxX = maxXTemp;
            if (maxYTemp > maxY)
                maxY = maxYTemp;
        }
        for (Attribute attribute : this.attributes) {
            maxXTemp = attribute.getEllipse().getMaxX();
            maxYTemp = attribute.getEllipse().getMaxY();
            if (maxXTemp > maxX)
                maxX = maxXTemp;
            if (maxYTemp > maxY)
                maxY = maxYTemp;
        }
        for (Relation relation : this.relations) {
            maxXTemp = relation.getPolygon().getMaxX();
            maxYTemp = relation.getPolygon().getMaxY();
            if (maxXTemp > maxX)
                maxX = maxXTemp;
            if (maxYTemp > maxY)
                maxY = maxYTemp;
        }
        for (Aggregation aggregation : this.aggregations) {
            maxXTemp = aggregation.polygon.getMaxX();
            maxYTemp = aggregation.polygon.getMaxY();
            if (maxXTemp > maxX)
                maxX = maxXTemp;
            if (maxYTemp > maxY)
                maxY = maxYTemp;
        }
        return new CustomPoint2D(maxX + 15 * Polygon.zoom, maxY + 15 * Polygon.zoom);
    }

    public void showOnCanvas(GraphicsContext gc, Color color) {
        this.polygon.setTopLeft(computeTopLeftCorner());
        this.polygon.setBottomRight(computeBottomRightCorner());
        this.polygon.setAvailablePoints();
        this.polygon.drawDashedWithoutFilling(gc, color);
    }

    public void saveCurrentCenters() {
        entitiesCenters.clear();
        attributesCenters.clear();
        relationsCenters.clear();
        for (Entity entity : this.entities) {
            this.entitiesCenters.add(new CustomPoint2D(entity.getPolygon().getCenter().getX() * Polygon.zoom, entity.getPolygon().getCenter().getY() * Polygon.zoom));
        }
        for (Attribute attribute : this.attributes) {
            this.attributesCenters.add(new CustomPoint2D(attribute.getEllipse().getCenter().getX() * Polygon.zoom, attribute.getEllipse().getCenter().getY() * Polygon.zoom));
        }
        for (Relation relation : this.relations) {
            this.relationsCenters.add(new CustomPoint2D(relation.getPolygon().getCenter().getX() * Polygon.zoom, relation.getPolygon().getCenter().getY() * Polygon.zoom));
        }
    }

    public void moveFigure(double distanceX, double distanceY) {
        for (int i = 0; i < entities.size(); i++) {
            entities.get(i).getPolygon().setCenterToMove(entitiesCenters.get(i).getX() + distanceX, entitiesCenters.get(i).getY() + distanceY);
        }
        for (int i = 0; i < attributes.size(); i++) {
            attributes.get(i).getEllipse().setCenterToMove(attributesCenters.get(i).getX() + distanceX, attributesCenters.get(i).getY() + distanceY);
        }
        for (int i = 0; i < relations.size(); i++) {
            relations.get(i).getPolygon().setCenterToMove(relationsCenters.get(i).getX() + distanceX, relationsCenters.get(i).getY() + distanceY);
        }
        for (Aggregation aggregation : this.aggregations) {
            aggregation.moveFigure(distanceX, distanceY);
        }
    }

}
