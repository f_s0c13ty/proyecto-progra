package model;

public enum EntityInheritanceType {

    DISJOINT,
    OVERLAPPING

}
