
package model;

/**
 * <code>RelationType</code> is an <code>enum</code> representing the 3
 * different types of relationship in an Entity-Relationship Diagram.
 * 
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 06.09.18 (Modified by: xxxx)
 */
public enum RelationType {
    UNARY,
    BINARY,
    TERNARY,
    QUATERNARY,
    QUINARY,
    SENARY
}
