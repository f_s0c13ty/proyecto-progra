package model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.figures.*;
import utilities.DateHelper;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Relation class: Represents the relationship among entities. It can
 * contains specific figures depending on the amount of entities involved in
 * the relationship.
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 08.09.18 (Modified by: xxxx)
 */
public class Relation implements Cloneable {

    private static int nameID = 1;

    private long id;

    //Verb that describes the relationship among the involved entities
    private String name;

    //The type of relationship, which is an enum RelationType
    private RelationType type;

    //The polygon associated to a relation
    private Polygon polygon;

    //The collection of unions and entities connected with each other
    private HashMap<ConnectingLine, Entity> entities;

    private HashMap<ConnectingLine, Aggregation> aggregations;

    private boolean isWeak;

    private boolean[] doubleLined = new boolean[]{false, false};

    private String[] cardinality = {"1", "N"};

    /**
     * Constructor set only name and type
     *
     * @param text
     * @param entities
     */
    public Relation(String text, ArrayList<Entity> entities, ArrayList<Aggregation> aggregations) {
        if (text.equals(""))
            this.name = "R" + nameID++;
        else
            this.name = text;
        this.id = DateHelper.getUTCTicks(Date.from(Instant.now()));
        this.entities = new HashMap<>();
        this.aggregations = new HashMap<>();
        this.isWeak = false;
        entities.forEach(entity -> this.entities.put(new ConnectingLine(), entity));
        aggregations.forEach(aggregation -> this.aggregations.put(new ConnectingLine(), aggregation));
    }

    public Relation() {
        this.entities = new HashMap<>();
        this.aggregations = new HashMap<>();
    }

    public static int getNameID() {
        return nameID;
    }

    public static void resetID() {
        nameID = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RelationType getType() {
        return type;
    }

    public void setType(RelationType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addEntity(Entity entity) {
        boolean wasUnary = !entities.isEmpty() && (entities.get(entities.keySet().toArray()[0]) == entities.get(entities.keySet().toArray()[1]));

        entities.put(new ConnectingLine(), entity);

        if(type.equals(RelationType.UNARY) && !aggregations.isEmpty()){
            removeAggregation(getAggregation(1));
        }

        this.setMyFigure((int) this.polygon.getCenter().getX(), (int) this.polygon.getCenter().getY());

        if (wasUnary) {
            if (entities.get(entities.keySet().toArray()[0]) == entities.get(entities.keySet().toArray()[1]) || entities.get(entities.keySet().toArray()[0]) == entities.get(entities.keySet().toArray()[2]))
                entities.remove(entities.keySet().toArray()[0]);
            else
                entities.remove(entities.keySet().toArray()[1]);

            this.setMyFigure((int) this.polygon.getCenter().getX(), (int) this.polygon.getCenter().getY());
        }
    }

    public void addAggregation(Aggregation aggregation){
        boolean wasUnary = !aggregations.isEmpty() && (aggregations.get(aggregations.keySet().toArray()[0]) == aggregations.get(aggregations.keySet().toArray()[1]));

        aggregations.put(new ConnectingLine(), aggregation);

        if(type.equals(RelationType.UNARY) && !entities.isEmpty()){
            removeEntity(getEntity(1));
        }

        this.setMyFigure((int) this.polygon.getCenter().getX(), (int) this.polygon.getCenter().getY());

        if (wasUnary) {
            if (aggregations.get(aggregations.keySet().toArray()[0]) == aggregations.get(aggregations.keySet().toArray()[1]) || aggregations.get(aggregations.keySet().toArray()[0]) == aggregations.get(aggregations.keySet().toArray()[2]))
                aggregations.remove(aggregations.keySet().toArray()[0]);
            else
                aggregations.remove(aggregations.keySet().toArray()[1]);

            this.setMyFigure((int) this.polygon.getCenter().getX(), (int) this.polygon.getCenter().getY());
        }
    }

    public void removeEntity(Entity entity) {
        for (ConnectingLine connectingLine : entities.keySet()) {
            if (entities.get(connectingLine) == entity) {
                entities.remove(connectingLine);
                if (type.equals(RelationType.UNARY) && aggregations.isEmpty())
                    entities.remove(entities.keySet().toArray()[0]);
                break;
            }
        }
        this.setMyFigure((int) this.polygon.getCenter().getX(), (int) this.polygon.getCenter().getY());
    }

    public void removeAggregation(Aggregation aggregation) {
        for (ConnectingLine connectingLine : aggregations.keySet()) {
            if (aggregations.get(connectingLine) == aggregation) {
                aggregations.remove(connectingLine);
                if (type.equals(RelationType.UNARY) && entities.isEmpty())
                    aggregations.clear();
                break;
            }
        }
        this.setMyFigure((int) this.polygon.getCenter().getX(), (int) this.polygon.getCenter().getY());
    }

    public HashMap<ConnectingLine, Entity> getEntities() {
        return this.entities;
    }

    public HashMap<ConnectingLine, Aggregation> getAggregations() {
        return this.aggregations;
    }

    public Entity getEntity(int index) {
        return entities.get(entities.keySet().toArray()[index]);
    }

    public Aggregation getAggregation(int index) {
        return aggregations.get(aggregations.keySet().toArray()[index]);
    }

    public Polygon getPolygon() {
        return this.polygon;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

    public boolean isWeak() {
        return isWeak;
    }

    public void setWeak(boolean weak) {
        isWeak = weak;
    }

    public void putEntities(ConnectingLine connectingLine, Entity entity) {
        entities.put(connectingLine, entity);
    }

    public String[] getCardinality() {
        return cardinality;
    }

    public void setCardinality(String cardinality, int index) {
        this.cardinality[index] = cardinality;
    }

    public boolean[] getDoubleLined() {
        return doubleLined;
    }

    public void setDoubleLined(boolean isDoubleLined, int index) {
        this.doubleLined[index] = isDoubleLined;
    }

    public Relation clone(ArrayList<Entity> entitiesCloned) {
        Relation obj = new Relation();
        obj.setId(this.id);
        obj.setName(this.name);
        obj.setType(this.type);
        obj.setPolygon(this.polygon.clone());
        obj.setWeak(this.isWeak);
        //Save connectingLines and entitiesCloned for the array list given
        entities.forEach((k, v) -> {
            for (Entity cloned : entitiesCloned) {
                if (cloned.getId() == v.getId()) {
                    obj.putEntities(k.clone(), cloned);
                    break;
                }
            }
        });
        return obj;
    }

    public void setMyFigure(double centerX, double centerY) {
        switch (this.entities.size() + this.aggregations.size()) {
            case 1:
                this.type = RelationType.UNARY;
                this.polygon = new Rhombus(this.name, centerX, centerY);
                if (!entities.isEmpty() && aggregations.isEmpty() && entities.size() < 2) {
                    this.entities.put(new ConnectingLine(), entities.get(entities.keySet().toArray()[0]));
                } else if(!aggregations.isEmpty() && entities.isEmpty() && aggregations.size() < 2){
                    this.aggregations.put(new ConnectingLine(), aggregations.get(aggregations.keySet().toArray()[0]));
                }
                break;
            case 2:
                this.type = RelationType.BINARY;
                this.polygon = new Rhombus(this.name, centerX, centerY);
                break;
            case 3:
                this.type = RelationType.TERNARY;
                this.polygon = new Triangle(this.name, centerX, centerY);
                break;
            case 4:
                this.type = RelationType.QUATERNARY;
                this.polygon = new Rhombus(this.name, centerX, centerY);
                break;
            case 5:
                this.type = RelationType.QUINARY;
                this.polygon = new Pentagon(this.name, centerX, centerY);
                break;
            case 6:
                this.type = RelationType.SENARY;
                this.polygon = new Hexagon(this.name, centerX, centerY);
                break;
            default:
                break;
        }
    }

    /**
     * Makes the polygon associated to this Relation appear on the canvas
     *
     * @param gc GraphicContext of the canvas
     */
    public void showFigureOnCanvas(GraphicsContext gc, Color color) {
        this.getPolygon().setAvailablePoints();
        if (!isWeak)
            this.polygon.draw(gc, color);
        else
            this.polygon.drawDoubleLined(gc, color);
    }

    /**
     * Makes the ConnectingLines associated to this Relation appear on the canvas
     *
     * @param gc GraphicContext of the canvas
     */
    public void showConnectingLinesOnCanvas(GraphicsContext gc) {
        int i = 0;
        for (Map.Entry<ConnectingLine, Entity> entry : entities.entrySet()) {
            Entity entity = entry.getValue();
            ConnectingLine.draw(gc, this, entity, cardinality[i]);
            setWeakConnectingLine(isWeak, entity, gc);
            setDoubleConnectingLine(entity, gc, doubleLined[i]);
            i++;
        }
        for (Map.Entry<ConnectingLine, Aggregation> entry : aggregations.entrySet()) {
            Aggregation aggregation = entry.getValue();
            ConnectingLine.draw(gc, this, aggregation, cardinality[i]);
            setDoubleConnectingLine(aggregation, gc, doubleLined[i]);
            i++;
        }
    }


    public void setWeakConnectingLine(boolean isWeak, Entity entity, GraphicsContext gc) {
        Object[] keySet = entities.keySet().toArray();
        if (isWeak && entities.size() == 2 && !entities.get(keySet[0]).equals(entities.get(keySet[1])))
            if (entity.isWeak())
                ConnectingLine.drawForWeakRelations(gc, this, entity);
    }

    public void setDoubleConnectingLine(Entity entity, GraphicsContext gc, boolean isDoubleConnected) {
        Object[] keySet = entities.keySet().toArray();
        if ((entities.size() == 2 && !entities.get(keySet[0]).equals(entities.get(keySet[1]))) || !aggregations.isEmpty())
            if (isDoubleConnected)
                ConnectingLine.drawForWeakRelations(gc, this, entity);
    }

    public void setDoubleConnectingLine(Aggregation aggregation, GraphicsContext gc, boolean isDoubleConnected) {
        if (isDoubleConnected)
            ConnectingLine.drawForWeakRelations(gc, this, aggregation);
    }
}


