package model;

/**
 * AttributeType enum: Type of the attribute of an entity (terms of databases)
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 21.10.18 (Modified by: Edward Becerra)
 */
public enum AttributeType {

    GENERIC,
    KEY,
    PARTIAL_KEY,
    MULTIVALUED,
    COMPOSITE,
    DERIVATIVE

}
