/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.MainWindowController;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import model.figures.Circle;
import model.figures.Polygon;
import utilities.CustomPoint2D;


/**
 * ConnectingLine class: Used to establish the connection between objects on the canvas
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 15.10.18 (Modified by: Edward Becerra) Line that remove a point from availableEntityCorners deleted
 */
public class ConnectingLine implements Cloneable {

    // Point in the relation
    private static CustomPoint2D relationConnectingPoint;

    // Point in the related entities
    private static CustomPoint2D[] entityConnectingPoint = new CustomPoint2D[2];

    private static CustomPoint2D[] attributeConnectingPoint = new CustomPoint2D[2];

    private static CustomPoint2D circleConnectingPoint;

    private static double lineWidth = 1 * Polygon.zoom;

    public ConnectingLine() {
    }

    /**
     * draw: Draw a line form start to end point
     *
     * @param gc
     */
    public static void draw(GraphicsContext gc, Relation relation, Entity entity, String cardinality) {
        shortestDistanceForRelations(relation, entity, false);
        gc.setLineWidth(lineWidth);
        gc.strokeLine(relationConnectingPoint.getX(), relationConnectingPoint.getY(), entityConnectingPoint[0].getX(), entityConnectingPoint[0].getY());
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFont(Font.font(12 * MainWindowController.scaleZoom));
        if (firstPointBigger(relationConnectingPoint, entityConnectingPoint[0]))
            gc.fillText(cardinality, relationConnectingPoint.getX() - 10, relationConnectingPoint.getY());
        else
            gc.fillText(cardinality, relationConnectingPoint.getX() + 10, relationConnectingPoint.getY());
    }

    public static void draw(GraphicsContext gc, Attribute attribute, Relation relation) {
        shortestDistanceForAttributeRelation(attribute, relation);
        gc.strokeLine(attributeConnectingPoint[0].getX(), attributeConnectingPoint[0].getY(), relationConnectingPoint.getX(), relationConnectingPoint.getY());
    }

    public static void draw(GraphicsContext gc, Attribute attribute, Entity entity) {
        shortestDistanceForAttributeEntity(attribute, entity);
        gc.strokeLine(attributeConnectingPoint[0].getX(), attributeConnectingPoint[0].getY(), entityConnectingPoint[0].getX(), entityConnectingPoint[0].getY());
    }

    public static void draw(GraphicsContext gc, Attribute attribute, Attribute attribute1) {
        shortestDistanceForAttributes(attribute, attribute1);
        gc.strokeLine(attributeConnectingPoint[0].getX(), attributeConnectingPoint[0].getY(), attributeConnectingPoint[1].getX(), attributeConnectingPoint[1].getY());
    }

    public static void draw(GraphicsContext gc, Entity entity, Circle circle) {
        // Parent Entity
        shortestDistanceFromParent(entity, circle);
        gc.strokeLine(entityConnectingPoint[0].getX(), entityConnectingPoint[0].getY(), circleConnectingPoint.getX(), circleConnectingPoint.getY());
    }

    public static void draw(GraphicsContext gc, Circle circle, Entity entity) {
        try {
            // Child entity
            shortestDistanceForChildren(circle, entity);
            Polygon.drawArcAndLine(gc, circleConnectingPoint, entityConnectingPoint[1]);
            gc.strokeLine(entityConnectingPoint[1].getX(), entityConnectingPoint[1].getY(), circleConnectingPoint.getX(), circleConnectingPoint.getY());
        } catch (NullPointerException ex) {

        }
    }

    public static void draw(GraphicsContext gc, Relation relation, Aggregation aggregation, String cardinality) {
        shortestDistanceForRelations(relation, aggregation, false);
        gc.setLineWidth(lineWidth);
        gc.strokeLine(relationConnectingPoint.getX(), relationConnectingPoint.getY(), entityConnectingPoint[0].getX(), entityConnectingPoint[0].getY());
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFont(Font.font(12 * MainWindowController.scaleZoom));
        if (firstPointBigger(relationConnectingPoint, entityConnectingPoint[0]))
            gc.fillText(cardinality, relationConnectingPoint.getX() - 10, relationConnectingPoint.getY());
        else
            gc.fillText(cardinality, relationConnectingPoint.getX() + 10, relationConnectingPoint.getY());
    }

    public static void drawForWeakRelations(GraphicsContext gc, Relation relation, Entity entity) {
        gc.setLineWidth(lineWidth);
        shortestDistanceForRelations(relation, entity, true);
        gc.strokeLine(relationConnectingPoint.getX() + 2, relationConnectingPoint.getY() + 2, entityConnectingPoint[0].getX() + 2, entityConnectingPoint[0].getY() + 2);
        gc.setLineWidth(lineWidth);
    }

    public static void drawForWeakRelations(GraphicsContext gc, Relation relation, Aggregation aggregation) {
        gc.setLineWidth(lineWidth);
        shortestDistanceForRelations(relation, aggregation, true);
        gc.strokeLine(relationConnectingPoint.getX() + 2, relationConnectingPoint.getY() + 2, entityConnectingPoint[0].getX() + 2, entityConnectingPoint[0].getY() + 2);
        gc.setLineWidth(lineWidth);
    }

    private static void shortestDistanceForRelations(Relation relation, Entity entity, boolean forWeakDistance) {
        if (forWeakDistance)
            relation.getPolygon().setAvailablePoints();

        if (!relation.getPolygon().getAvailableCorners().isEmpty() && !entity.getPolygon().getAvailableCorners().isEmpty()) {
            CustomPoint2D relationPoint = relation.getPolygon().getAvailableCorners().get(0);
            CustomPoint2D entityPoint = entity.getPolygon().getAvailableCorners().get(0);
            double minimumDistance = relationPoint.distance(entityPoint);
            double calculatedDistance;
            for (CustomPoint2D rPoint : relation.getPolygon().getAvailableCorners()) {
                for (CustomPoint2D ePoint : entity.getPolygon().getAvailableCorners()) {
                    calculatedDistance = rPoint.distance(ePoint);
                    if (calculatedDistance < minimumDistance) {
                        relationPoint = rPoint;
                        entityPoint = ePoint;
                        minimumDistance = calculatedDistance;
                    }
                }
            }
            relationConnectingPoint = relationPoint;
            if (relation.getEntities().get(0) == relation.getEntities().get(1) && !forWeakDistance)
                relation.getPolygon().getAvailableCorners().remove(relationPoint);
            entityConnectingPoint[0] = entityPoint;
        }
    }

    private static void shortestDistanceForAttributeEntity(Attribute attribute, Entity entity) {
        if (!attribute.getEllipse().getAvailableCorners().isEmpty() && !entity.getPolygon().getAvailableCorners().isEmpty()) {
            CustomPoint2D[] points = {attribute.getEllipse().getAvailableCorners().get(0), entity.getPolygon().getAvailableCorners().get(0)};
            double minimumDistance = points[0].distance(points[1]);
            double calculatedDistance;
            for (CustomPoint2D attributePoint : attribute.getEllipse().getAvailableCorners()) {
                for (CustomPoint2D entityPoint : entity.getPolygon().getAvailableCorners()) {
                    calculatedDistance = attributePoint.distance(entityPoint);
                    if (calculatedDistance < minimumDistance) {
                        points[0] = attributePoint;
                        points[1] = entityPoint;
                        minimumDistance = calculatedDistance;
                    }
                }
            }
            attributeConnectingPoint[0] = points[0];
            entityConnectingPoint[0] = points[1];
        }
    }

    private static void shortestDistanceForAttributeRelation(Attribute attribute, Relation relation) {
        relation.getPolygon().setAvailablePoints();
        if (!attribute.getEllipse().getAvailableCorners().isEmpty() && !relation.getPolygon().getAvailableCorners().isEmpty()) {
            CustomPoint2D[] points = {attribute.getEllipse().getAvailableCorners().get(0), relation.getPolygon().getAvailableCorners().get(0)};
            double minimumDistance = points[0].distance(points[1]);
            double calculatedDistance;
            for (CustomPoint2D attributePoint : attribute.getEllipse().getAvailableCorners()) {
                for (CustomPoint2D relationPoint : relation.getPolygon().getAvailableCorners()) {
                    calculatedDistance = attributePoint.distance(relationPoint);
                    if (calculatedDistance < minimumDistance) {
                        points[0] = attributePoint;
                        points[1] = relationPoint;
                        minimumDistance = calculatedDistance;
                    }
                }
            }
            attributeConnectingPoint[0] = points[0];
            relationConnectingPoint = points[1];
        }
    }

    private static void shortestDistanceForAttributes(Attribute attribute, Attribute attribute1) {
        if (!attribute.getEllipse().getAvailableCorners().isEmpty() && !attribute1.getEllipse().getAvailableCorners().isEmpty()) {
            CustomPoint2D[] points = {attribute.getEllipse().getAvailableCorners().get(0), attribute1.getEllipse().getAvailableCorners().get(0)};
            double minimumDistance = points[0].distance(points[1]);
            double calculatedDistance;
            for (CustomPoint2D attributePoint : attribute.getEllipse().getAvailableCorners()) {
                for (CustomPoint2D attribute1Point : attribute1.getEllipse().getAvailableCorners()) {
                    calculatedDistance = attributePoint.distance(attribute1Point);
                    if (calculatedDistance < minimumDistance) {
                        points[0] = attributePoint;
                        points[1] = attribute1Point;
                        minimumDistance = calculatedDistance;
                    }
                }
            }
            attributeConnectingPoint[0] = points[0];
            attributeConnectingPoint[1] = points[1];
        }
    }

    private static void shortestDistanceFromParent(Entity entity, Circle circle) {
        CustomPoint2D circlePoint = null, entityPoint = null;
        double minimumDistance = circle.getAnchorPoints()[0].distance(entity.getPolygon().getAnchorPoints()[0]);
        double calculatedDistance;
        for (CustomPoint2D cPoint : circle.getAnchorPoints()) {
            for (CustomPoint2D ePoint : entity.getPolygon().getAnchorPoints()) {
                calculatedDistance = cPoint.distance(ePoint);
                if (calculatedDistance <= minimumDistance) {
                    minimumDistance = calculatedDistance;
                    circlePoint = cPoint;
                    entityPoint = ePoint;
                }
            }
        }
        circleConnectingPoint = circlePoint;
        entityConnectingPoint[0] = entityPoint;
    }

    private static void shortestDistanceForChildren(Circle circle, Entity entity) {
        CustomPoint2D circlePoint = circle.getAnchorPoints()[0], entityPoint = entity.getPolygon().getAnchorPoints()[0];
        double minimumDistance = circle.getAnchorPoints()[0].distance(entity.getPolygon().getAnchorPoints()[0]);
        double calculatedDistance;
        for (CustomPoint2D cPoint : circle.getAnchorPoints()) {
            for (CustomPoint2D ePoint : entity.getPolygon().getAnchorPoints()) {
                calculatedDistance = cPoint.distance(ePoint);
                if (calculatedDistance < minimumDistance) {
                    minimumDistance = calculatedDistance;
                    circlePoint = cPoint;
                    entityPoint = ePoint;
                }
            }
        }
        circleConnectingPoint = circlePoint;
        entityConnectingPoint[1] = entityPoint;
    }

    private static void shortestDistanceForRelations(Relation relation, Aggregation aggregation, boolean forWeakDistance) {
        if (forWeakDistance)
            relation.getPolygon().setAvailablePoints();

        if (!relation.getPolygon().getAvailableCorners().isEmpty() && !aggregation.getPolygon().getAvailableCorners().isEmpty()) {
            CustomPoint2D relationPoint = relation.getPolygon().getAvailableCorners().get(0);
            CustomPoint2D entityPoint = aggregation.getPolygon().getAvailableCorners().get(0);
            double minimumDistance = relationPoint.distance(entityPoint);
            double calculatedDistance;
            for (CustomPoint2D rPoint : relation.getPolygon().getAvailableCorners()) {
                for (CustomPoint2D ePoint : aggregation.getPolygon().getAvailableCorners()) {
                    calculatedDistance = rPoint.distance(ePoint);
                    if (calculatedDistance < minimumDistance) {
                        relationPoint = rPoint;
                        entityPoint = ePoint;
                        minimumDistance = calculatedDistance;
                    }
                }
            }
            relationConnectingPoint = relationPoint;
            if (relation.getEntities().get(0) == relation.getEntities().get(1) && !forWeakDistance)
                relation.getPolygon().getAvailableCorners().remove(relationPoint);
            entityConnectingPoint[0] = entityPoint;
        }
    }

    public static void setRelationConnectingPoint(CustomPoint2D relationConnectingPoint) {
        ConnectingLine.relationConnectingPoint = relationConnectingPoint;
    }

    public static void setEntityConnectingPoint(CustomPoint2D[] entityConnectingPoint) {
        ConnectingLine.entityConnectingPoint = entityConnectingPoint;
    }

    public static void setAttributeConnectingPoint(CustomPoint2D[] attributeConnectingPoint) {
        ConnectingLine.attributeConnectingPoint = attributeConnectingPoint;
    }

    public static void setCircleConnectingPoint(CustomPoint2D circleConnectingPoint) {
        ConnectingLine.circleConnectingPoint = circleConnectingPoint;
    }

    private static boolean firstPointBigger(CustomPoint2D one, CustomPoint2D two) {
        if ((one.getX() + one.getY()) > (two.getX() + two.getY())) {
            return true;
        }
        return false;
    }

    public ConnectingLine clone() {
        ConnectingLine obj = new ConnectingLine();
        if (relationConnectingPoint != null) obj.setRelationConnectingPoint(relationConnectingPoint.clone());
        if (entityConnectingPoint != null) obj.setEntityConnectingPoint(entityConnectingPoint.clone());
        if (attributeConnectingPoint != null) obj.setAttributeConnectingPoint(attributeConnectingPoint.clone());
        if (circleConnectingPoint != null) obj.setCircleConnectingPoint(circleConnectingPoint.clone());
        return obj;
    }

}
