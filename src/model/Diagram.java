package model;

import application.Main;
import java.util.ArrayList;

/**
 * Diagram class: Contains all the data that will be draw on canvas
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 08.09.18 (Modified by: xxxx)
 */
public class Diagram implements Cloneable{
    private ArrayList<Entity> entities;
    private ArrayList<Relation> relations;
    private ArrayList<Attribute> attributes;
    private ArrayList<Aggregation> aggregations;
    private int width;
    private int height;

    public Diagram() {
        this.entities = new ArrayList<>();
        this.relations = new ArrayList<>();
        this.attributes = new ArrayList<>();
        this.aggregations = new ArrayList<>();
        this.width = 0;
        this.height = 0;
    }

    public void clear() {
        this.entities.clear();
        this.relations.clear();
        this.attributes.clear();
        this.aggregations.clear();
        Entity.resetID();
        Relation.resetID();
        Attribute.resetID();
        Aggregation.resetID();
    }

    public Diagram clone(){
        Diagram obj = new Diagram();
        entities.forEach(entity -> obj.entities.add(entity.clone()));
        obj.entities.forEach(entity -> {
            if(entity.getInheritance() != null)
                entity.getInheritance().setClonedChildren(obj.entities);
        });

        relations.forEach(relation -> obj.relations.add(relation.clone(obj.getEntities())));
        attributes.forEach(attribute -> obj.attributes.add(attribute.clone(obj.entities,obj.relations)));

        obj.attributes.forEach(attribute -> {
            if(attribute.getAttributeType().equals(AttributeType.COMPOSITE))
                attribute.setClonedAttributes(obj.attributes);
        });

        obj.setHeight(this.height);
        obj.setWidth(this.width);
        return obj;
    }

    /**
     * Computes how wide the used area of the canvas is. This method is used
     * for exporting an image with an adecuate resolution
     *
     * @return the needed width in pixels that the diagram needs to be drawn
     */
    public int computeWidth() {
        int width = 0;
        for(Entity entity : entities) {
            if(entity.getPolygon().getMaxX() > width) {
                width = entity.getPolygon().getMaxX();
            }
            if(entity.getInheritance() != null && entity.getInheritance().getFigure().getMaxX() > width)
                width = entity.getInheritance().getFigure().getMaxX();
        }
        for(Relation relation : relations) {
            if(relation.getPolygon().getMaxX() > width) {
                width = relation.getPolygon().getMaxX();
            }
        }
        for(Attribute attribute : attributes) {
            if(attribute.getEllipse().getMaxX() > width) {
                width = attribute.getEllipse().getMaxX();
            }
        }
        return width;
    }

    /**
     * Computes how tall the used area of the canvas is. This method is used
     * for exporting an image with an adecuate resolution
     *
     * @return the needed height in pixels that the diagram needs to be drawn
     *
     */
    public int computeHeight() {
        int height = 0;
        for(Entity entity : entities) {
            if(entity.getPolygon().getMaxY() > height) {
                height = entity.getPolygon().getMaxY();
            }
            if(entity.getInheritance() != null && entity.getInheritance().getFigure().getMaxY() > height)
                height = entity.getInheritance().getFigure().getMaxY();
        }
        for(Relation relation : relations) {
            if(relation.getPolygon().getMaxY() > height) {
                height = relation.getPolygon().getMaxY();
            }
        }
        for(Attribute attribute : attributes) {
            if(attribute.getEllipse().getMaxY() > height) {
                height = attribute.getEllipse().getMaxY();
            }
        }
        return height;
    }

    /**
     * eraseEntity: Deletes every occurrence of an entity
     * Leaves the attributes on the diagram so they can be reuse
     * @param entity
     */
    public void eraseEntity(Entity entity){
        //Main.saveDiagramState();
        entities.remove(entity);
        entities.forEach(entity1 -> {
            if(entity1.getInheritance() != null && entity1.getInheritance().getChildren().contains(entity))
                entity1.getInheritance().removeChildren(entity);
        });
        relations.forEach(relation -> {
            if(relation.getEntities().containsValue(entity))
                relation.removeEntity(entity);
        });
        attributes.forEach(attribute -> {
            if(attribute.getEntityConnected() == entity)
                attribute.setEntityConnected(null);
        });
        Aggregation toDelete = null;
        for (Aggregation aggregation : aggregations) {
            if (aggregation.getEntities().contains(entity))
                aggregation.getEntities().remove(entity);
            if(aggregation.getEntities().contains(entity))
                aggregation.getEntities().remove(entity);

            if(aggregation.getEntities().isEmpty())
                toDelete = aggregation;
        }
        if(toDelete != null)
            eraseAggregation(toDelete);
    }

    /**
     * eraseRelation: Deletes every occurrence of a relation
     * @param relation
     */
    public void eraseRelation(Relation relation){
        //Main.saveDiagramState();
        relations.remove(relation);
        attributes.forEach(attribute -> {
            if(attribute.getRelationConnected() != null && attribute.getRelationConnected().equals(relation))
                attribute.setRelationConnected(null);
        });
        Aggregation toDelete = null;
        for (Aggregation aggregation : aggregations) {
            if (aggregation.getRelations().contains(relation))
                aggregation.getRelations().remove(relation);
            if(aggregation.getRelations().isEmpty())
                toDelete = aggregation;
        }
        if(toDelete != null)
            eraseAggregation(toDelete);
    }

    /**
     * eraseRelation: Deletes every occurrence of an attribute
     * @param attribute
     */
    public void eraseAttribute(Attribute attribute){
        //Main.saveDiagramState();
        attributes.remove(attribute);

        for (Attribute attribute1 : attributes) {
            if (attribute1.getAttributeType().equals(AttributeType.COMPOSITE) && attribute1.getAttributes().contains(attribute)) {
                attribute1.removeAttribute(attribute);
                break;
            }
        }
        for (Aggregation aggregation : aggregations) {
            if (aggregation.getAttributes().contains(attribute))
                aggregation.getAttributes().remove(attribute);
        }
    }

    public void eraseInheritance(EntityInheritance inheritance) {
        //Main.saveDiagramState();
        inheritance.getChildren().forEach((child) -> child.setChild(false));
        inheritance.getParent().removeInheritance();
    }

    public void eraseAggregation(Aggregation aggregation) {
        // Main.saveDiagramState();
        aggregations.remove(aggregation);
        relations.forEach(relation -> {
            if(relation.getAggregations().containsValue(aggregation))
                relation.removeAggregation(aggregation);
        });
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }

    public ArrayList<Relation> getRelations() {
        return relations;
    }

    public ArrayList<Aggregation> getAggregations() {
        return aggregations;
    }

    public void addRelation(Relation relation) {
        this.relations.add(relation);
    }

    public void addEntity(Entity entity) {
        this.entities.add(entity);
    }

    public void addAttribute(Attribute attribute) {
        this.attributes.add(attribute);
    }

    public void addAggregation(Aggregation aggregation) {
        this.aggregations.add(aggregation);
    }

}
