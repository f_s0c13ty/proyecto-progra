package model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.figures.Rectangle;
import utilities.DateHelper;

import java.time.Instant;
import java.util.Date;

/**
 * Entity Class
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 18.09.18 (Modified by: xxxx)
 */
public class Entity implements Cloneable {
    private static int nameID = 1;
    private long id;
    private String name;
    private Rectangle polygon;
    private boolean isWeak;
    private boolean isChild;
    private EntityInheritance inheritance;


    public Entity(String name) {
        if (name.equals(""))
            this.name = "E" + nameID++;
        else
            this.name = name;
        this.id = DateHelper.getUTCTicks(Date.from(Instant.now()));
        this.isWeak = false;
        this.isChild = false;
        this.inheritance = null;
    }

    public Entity(String name, Rectangle polygon, boolean isWeak, boolean isChild) {
        this.name = name;
        this.polygon = polygon;
        this.isWeak = isWeak;
        this.isChild = isChild;
    }

    public static int getNameID() {
        return nameID;
    }

    public static void resetID() {
        nameID = 1;
    }

    public void setPolygon(double centerX, double centerY) {
        this.polygon = new Rectangle(this.name, centerX, centerY);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rectangle getPolygon() {
        return this.polygon;
    }

    public boolean isWeak() {
        return isWeak;
    }

    public void setWeak(boolean weak) {
        isWeak = weak;
    }

    public boolean isChild() {
        return this.isChild;
    }

    public void setChild(boolean isChild) {
        this.isChild = isChild;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * showOnCanvas: It makes the figure associated to this class appear on canvas
     * Needs the GraphicsContext from the view.
     *
     * @param gc
     */
    public void showOnCanvas(GraphicsContext gc, Color color) {
        this.getPolygon().setAvailablePoints();
        if (!isWeak)
            this.polygon.draw(gc, color);
        else
            this.polygon.drawDoubleLined(gc, color);
    }

    public EntityInheritance getInheritance() {
        return this.inheritance;
    }

    public void setInheritance(EntityInheritance inheritance) {
        this.inheritance = inheritance;
    }

    public void setInheritance(EntityInheritanceType type, double centerX, double centerY) {
        this.inheritance = new EntityInheritance(type, centerX, centerY);
    }

    public void removeInheritance() {
        this.inheritance = null;
    }

    public Entity clone() {
        Entity obj = new Entity(this.name, (Rectangle) polygon.clone(), this.isWeak, this.isChild);
        obj.setId(this.id);
        if (inheritance != null) obj.setInheritance(this.inheritance.clone(obj));
        return obj;
    }

}
