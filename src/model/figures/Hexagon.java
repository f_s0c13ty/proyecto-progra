package model.figures;

import utilities.CustomPoint2D;

/**
 * Hexagon class
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 0X.09.18 (Modified by: xxxx)
 */
public class Hexagon extends Polygon {

    public Hexagon(String labelText, double centerX, double centerY) {
        super(labelText, 20);
        this.offset = 0;
        this.corners = new CustomPoint2D[6];
        this.anchorPoints = new CustomPoint2D[6];
        setCenter(centerX / zoom, centerY / zoom);
        build();
    }


}
