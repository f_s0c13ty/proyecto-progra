package model.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import utilities.CustomPoint2D;

/**
 * Triangle class
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 0X.09.18 (Modified by: xxxx)
 */
public class Triangle extends Polygon implements Cloneable{

    public Triangle(String labelText, double centerX, double centerY) {
        super(labelText, 20);
        this.offset = 0;
        this.corners = new CustomPoint2D[3];
        this.anchorPoints = new CustomPoint2D[3];
        setCenter(centerX / zoom, centerY / zoom);
        build();
    }

    //Even though the triangle could have been drawn using the same method as
    //the other polygons, it was easier to override it in order to have more
    //control over the position of the label in the figure.
    /**
     * Defines the position of every corner of the polygon
     */
    @Override
    protected void computeCorners() {
        corners[0] = new CustomPoint2D(center.getX() + radius, center.getY());
        corners[1] = new CustomPoint2D(center.getX() - radius, center.getY() - (2*radius) / Math.sqrt(3));
        corners[2] = new CustomPoint2D(center.getX() - radius, center.getY() + (2*radius) / Math.sqrt(3));
    }

    /**
     * Draws the shape using double lines.
     *
     * @param gc GraphicsContext where to draw
     */
    @Override
    public void drawDoubleLined(GraphicsContext gc, Color color) {
        applyZoom();
        fill(gc);
        drawSides(gc, color);
        this.radius -= spaceBetweenDoubleLines;
        this.setCenter(center.getX() - 2, center.getY());
        computeCorners();
        drawSides(gc, color);
        this.radius += spaceBetweenDoubleLines;
        this.setCenter(center.getX() + 2, center.getY());
        computeControlPoints();
        computeCorners();
        //computeAnchorPoints();
        putTextOnCanvas(gc);
    }

    // Change something, just to be able to commit

}