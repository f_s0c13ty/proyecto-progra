package model.figures;

import utilities.CustomPoint2D;

/**
 * Rectangle class
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 10.09.18 (Modified by: xxxx)
 */
public class Rectangle extends Polygon implements Cloneable {

    public Rectangle(String labelText, double centerX, double centerY) {
        super(labelText, 10);
        this.offset = 0;
        this.spaceBetweenDoubleLines = 3;
        this.corners = new CustomPoint2D[4];
        this.anchorPoints = new CustomPoint2D[8];
        setCenter(centerX / zoom, centerY / zoom);
        build();
    }

    public Rectangle(String labelText, CustomPoint2D topLeft, CustomPoint2D bottomRight) {
        super(labelText, 10);
        this.corners = new CustomPoint2D[4];
        this.corners[0] = topLeft;
        this.corners[1] = new CustomPoint2D(bottomRight.getX(), topLeft.getY());
        this.corners[2] = bottomRight;
        this.corners[3] = new CustomPoint2D(topLeft.getX(), bottomRight.getY());
        this.controlPoints = corners;
        this.anchorPoints = new CustomPoint2D[8];
        this.center = topLeft.midpoint(bottomRight);
        computeAnchorPoints();
    }

    public CustomPoint2D getTopLeft() {
        return this.corners[0];
    }

    public void setTopLeft(CustomPoint2D topLeft) {
        this.corners[0] = topLeft;
        this.corners[1].setY(topLeft.getY());
        this.corners[3].setX(topLeft.getX());
        this.controlPoints = corners;
        this.center = corners[0].midpoint(corners[2]);
        computeAnchorPoints();
    }

    public CustomPoint2D getBottomRight() {
        return this.corners[2];
    }

    public void setBottomRight(CustomPoint2D bottomRight) {
        this.corners[2] = bottomRight;
        this.corners[1].setX(bottomRight.getX());
        this.corners[3].setY(bottomRight.getY());
        this.controlPoints = corners;
        this.center = corners[0].midpoint(corners[2]);
        computeAnchorPoints();
    }

    public void setTopLeft(double x, double y) {
        setTopLeft(new CustomPoint2D(x, y));
    }

    public void setBottomRight(double x, double y) {
        setBottomRight(new CustomPoint2D(x, y));
    }

    @Override
    public void computeRadius() {
        super.computeRadius();
        //Define a minimum radius for any rectangle
        if (this.radius < 40) {
            this.radius = 40;
        }
        this.radius2 = 25;
    }


    @Override
    protected void computeCorners() {
        this.corners[0] = new CustomPoint2D(this.center.getX() - radius, this.center.getY() - radius2);
        this.corners[1] = new CustomPoint2D(this.center.getX() + radius, this.center.getY() - radius2);
        this.corners[2] = new CustomPoint2D(this.center.getX() + radius, this.center.getY() + radius2);
        this.corners[3] = new CustomPoint2D(this.center.getX() - radius, this.center.getY() + radius2);
    }


    @Override
    protected void computeAnchorPoints() {
        for (int i = 0, j = 0, k = 0; i < this.anchorPoints.length; i++) {
            if (i % 2 == 0) {
                this.anchorPoints[i] = this.corners[j];
                j++;
            } else {
                if (k == this.corners.length - 1) {
                    this.anchorPoints[i] = this.corners[k].midpoint(this.corners[0]);
                } else {
                    this.anchorPoints[i] = this.corners[k].midpoint(this.corners[k + 1]);
                }
                k++;
            }
        }
    }

    public boolean clickOnBorders(double x, double y) {
        final int margin = 8;
        if (x >= corners[0].getX() - margin && x <= corners[1].getX() + margin && y >= corners[0].getY() - margin && y <= corners[0].getY() + margin)
            return true;
        if (x >= corners[1].getX() - margin && x <= corners[1].getX() + margin && y >= corners[1].getY() - margin && y <= corners[2].getY() + margin)
            return true;
        if (x >= corners[3].getX() - margin && x <= corners[2].getX() + margin && y >= corners[2].getY() - margin && y <= corners[2].getY() + margin)
            return true;
        if (x >= corners[3].getX() - margin && x <= corners[3].getX() + margin && y >= corners[0].getY() - margin && y <= corners[3].getY() + margin)
            return true;
        return false;
    }

}