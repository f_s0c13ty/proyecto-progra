
package model.figures;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import utilities.CustomPoint2D;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Abstract Polygon class: Sets the attributes that every child class should have.
 * Declares the draw functions in order to be implemented by the future children
 * classes
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 06.09.18 (Modified by: xxxx)
 */
public abstract class Polygon implements Cloneable {
    protected CustomPoint2D[] corners;
    protected CustomPoint2D[] controlPoints;
    protected CustomPoint2D[] anchorPoints;
    protected CustomPoint2D center;
    protected int radius;
    protected int radius2;
    protected int spaceBetweenDoubleLines;
    protected double offset;
    protected Text label;
    protected int textMargin;
    protected ArrayList<CustomPoint2D> availableCorners; // List of corners that are not used by a ConnectingLine
    protected boolean underlinedLabel;
    protected boolean underlinedLabelDashed;
    public static double zoom = 1;

    public Polygon(String label, int textMargin) {
        this.label = new Text(label);
        this.textMargin = textMargin;
        this.controlPoints = new CustomPoint2D[4];
        this.availableCorners = new ArrayList<>();
        this.spaceBetweenDoubleLines = 5;
        this.radius2 = 0;
    }

    /****************************GETTERS/SETTERS*********************************/

    public int getMinX() {
        int minX = (int) this.corners[0].getX();
        for (int i = 0; i < this.corners.length; i++) {
            CustomPoint2D corner = this.corners[i];
            if (corner.getX() < minX)
                minX = (int) corner.getX();
        }
        return minX;
    }

    public int getMinY() {
        int minY = (int) this.corners[0].getY();
        for (int i = 0; i < this.corners.length; i++) {
            CustomPoint2D corner = this.corners[i];
            if (corner.getY() < minY)
                minY = (int) corner.getY();
        }
        return minY;
    }


    /**
     * Finds the x coordinate of the right-most corner
     *
     * @return x coordinate of the corner located most to the right
     */
    public int getMaxX() {
        int maxX = 0;
        for (int i = 0; i < this.corners.length; i++) {
            CustomPoint2D corner = this.corners[i];
            if (corner.getX() > maxX) {
                maxX = (int) corner.getX();
            }
        }
        return maxX;
    }

    /**
     * Finds the y coordinate of the bottom-most corner
     *
     * @return y coordinate of the corner located most near the bottom
     */
    public int getMaxY() {
        int maxY = 0;
        for (int i = 0; i < this.corners.length; i++) {
            CustomPoint2D corner = this.corners[i];
            if (corner.getY() > maxY) {
                maxY = (int) corner.getY();
            }
        }
        return maxY;
    }

    /**
     * Sets a new center to the figure. Also, updates its corners and control
     * points.
     *
     * @param x x coordinate of the new center
     * @param y y coordinate of the new center
     */
    public void setCenterToMove(double x, double y) {
        this.center = new CustomPoint2D(x / zoom, y / zoom);
    }

    public void setCenter(double x, double y) {
        this.center = new CustomPoint2D(x, y);
    }

    public ArrayList<CustomPoint2D> getAvailableCorners() {
        return this.availableCorners;
    }

    public void setAvailablePoints() {
        this.availableCorners = this.copyAnchors();
    }

    public void setLabel(String label) {
        this.label.setText(label);
    }

    public CustomPoint2D[] getAnchorPoints() {
        return this.anchorPoints;
    }

    public CustomPoint2D getCenter() {
        return this.center;
    }

    public Text getLabel() {
        return this.label;
    }

    public boolean isUnderlinedLabel() {
        return underlinedLabel;
    }

    public void setUnderlinedLabel(boolean underlineLabel) {
        this.underlinedLabel = underlineLabel;
    }

    public boolean isUnderlinedLabelDashed() {
        return underlinedLabelDashed;
    }

    public void setUnderlinedLabelDashed(boolean underlinedLabelDashed) {
        this.underlinedLabelDashed = underlinedLabelDashed;
    }
    /**************************END GETTERS/SETTERS*********************************/

    /**
     * Draws a line with an arc which is always concave with respect to the
     * starting point of the line
     *
     * @param gc        GraphicContext where to do the drawing
     * @param parentPos the starting point of the line
     * @param childPos  the ending point of the line
     */
    public static void drawArcAndLine(GraphicsContext gc, CustomPoint2D parentPos, CustomPoint2D childPos) {
        try {
            double height = 30 * zoom, width = height;
            double posX, posY;
            double angle;
            CustomPoint2D horizontalUnitVector = new CustomPoint2D(1, 0);
            CustomPoint2D arcPos = parentPos.midpoint(childPos);
            posY = arcPos.getY();
            posX = arcPos.getX();

            //Draw the line
            gc.strokeLine(parentPos.getX(), parentPos.getY(), childPos.getX(), childPos.getY());

            //Compute the angle of the arc
            angle = horizontalUnitVector.angle(childPos.subtract(parentPos));
            if (parentPos.getY() <= childPos.getY()) {
                angle = 270 - angle;
            } else {
                angle -= 90;
            }

            //Adjust the positioning of the arc
            posX -= width / 2;
            posY -= height / 2;

            //Draw the arc
            gc.strokeArc(posX, posY, width, height, angle, 180, ArcType.OPEN);
        } catch (NullPointerException ex) {

        }
    }

    /**
     * Calculates the controls points of a figure, that is to say, the 4 vertexes
     * that define the imaginary rectangular frame surrounding every figure
     */
    protected void computeControlPoints() {
        controlPoints[0] = new CustomPoint2D(this.center.getX() - this.radius, this.center.getY() - this.radius2);
        controlPoints[1] = new CustomPoint2D(this.center.getX() + this.radius, this.center.getY() - this.radius2);
        controlPoints[2] = new CustomPoint2D(this.center.getX() + this.radius, this.center.getY() + this.radius2);
        controlPoints[3] = new CustomPoint2D(this.center.getX() - this.radius, this.center.getY() + this.radius2);
    }

    /**
     * Calculates the size of the angle between any pair of adjacent corners and
     * the center of the polygon.
     *
     * @return the computed angle
     */

    protected double computeAngle() {
        return 2 * Math.PI / corners.length;
    }

    /**
     * Defines the position of every corner of the polygon
     */
    protected void computeCorners() {
        double computedAngle = computeAngle();
        for (int i = 0; i < corners.length; i++) {
            double angle = computedAngle * i + this.offset;
            double x = this.radius * Math.cos(angle);
            double y = this.radius2 * Math.sin(angle);
            corners[i] = new CustomPoint2D(x + center.getX(), y + center.getY());
        }
    }

    /**
     * Paints the  control points of the figure in the canvas
     *
     * @param gc GraphicsContext of the Canvas
     */
    public void showCorners(GraphicsContext gc) {
        gc.setStroke(Color.RED);
        gc.setLineWidth(5 * zoom);
        for (CustomPoint2D controlPoint : this.corners) {
            gc.strokeLine(controlPoint.getX(), controlPoint.getY(), controlPoint.getX(), controlPoint.getY());
        }
        // restore default stroke settings
        gc.setLineWidth(1);
        gc.setStroke(Color.BLACK);
    }

    /**
     * Draws the shape and puts its label into the canvas
     *
     * @param gc GraphicsContext where to draw
     */
    public void draw(GraphicsContext gc, Color color) {
        applyZoom();
        fill(gc);
        drawSides(gc, color);
        putTextOnCanvas(gc);
        removeZoom();
    }

    /**
     * Draws the shape using double lines.
     *
     * @param gc GraphicsContext where to draw
     */
    public void drawDoubleLined(GraphicsContext gc, Color color) {
        applyZoom();
        gc.setLineWidth(1 * zoom);
        int aux = this.radius;
        int aux2 = this.radius2;
        fill(gc);
        drawSides(gc, color);
        this.radius -= spaceBetweenDoubleLines;
        this.radius2 -= spaceBetweenDoubleLines;
        computeCorners();
        drawSides(gc, color);
        this.radius = aux;
        this.radius2 = aux2;
        computeCorners();
        putTextOnCanvas(gc);
        removeZoom();
    }

    /**
     * Draws the shape with dotted lines and puts its label into the canvas
     *
     * @param gc GraphicsContext where to draw
     */
    public void drawDashed(GraphicsContext gc, Color color) {
        applyZoom();
        gc.setLineWidth(2 * zoom);
        fill(gc);
        drawSidesDashed(gc, color);
        putTextOnCanvas(gc);
        removeZoom();
    }

    public void drawDashedWithoutFilling(GraphicsContext gc, Color color) {
        gc.setLineWidth(2 * zoom);
        drawSidesDashed(gc, color);
        putTextOnCanvas(gc, this.corners[0].getX() + 15 * zoom, this.corners[0].getY() + 15 * zoom);
    }
    private ArrayList<CustomPoint2D> copyAnchors() {
        ArrayList<CustomPoint2D> array = new ArrayList<>();
        array.addAll(Arrays.asList(this.anchorPoints));
        return array;
    }

    public boolean doesContainsCoords(double coordX, double coordY) {
        return (this.controlPoints[0].getX() <= coordX && this.controlPoints[2].getX() >= coordX)
                && (this.controlPoints[0].getY() <= coordY && this.controlPoints[2].getY() >= coordY);
    }

    public void computeRadius() {
        this.radius = (int) ((this.label.getLayoutBounds().getWidth() + 2 * this.textMargin) / 2);
        this.radius2 = radius;
    }

    /**
     * putTextOnCanvas: Sets the text on the canvas
     *
     * @param gc graphicsContext object of the canvas
     */
    protected void putTextOnCanvas(GraphicsContext gc) {
        gc.setLineWidth(1 * zoom);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFont(Font.font(12 * zoom));
        gc.fillText(label.getText(), this.center.getX(), this.center.getY());
        if (isUnderlinedLabel()) {
            underlineLabel(gc);
        } else if (isUnderlinedLabelDashed()) {
            underlineLabelDashed(gc);
        }
    }

    protected void putTextOnCanvas(GraphicsContext gc, double x, double y) {
        gc.setLineWidth(1 * zoom);
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFont(Font.font(12 * zoom));
        gc.fillText(label.getText(), x, y);
        if (isUnderlinedLabel()) {
            underlineLabel(gc);
        } else if (isUnderlinedLabelDashed()) {
            underlineLabelDashed(gc);
        }
    }

    /**
     * Draws each side of the polygon
     *
     * @param gc GraphicsContext where to draw the polygon
     */
    protected void drawSides(GraphicsContext gc) {
        gc.setLineWidth(2);
        for (int i = 0, j = 1; i < corners.length; i++, j++) {
            if (j == corners.length) {
                j = 0;
            }
            gc.strokeLine(corners[i].getX(), corners[i].getY(), corners[j].getX(), corners[j].getY());
        }
    }

    /**
     * Draws each side of the polygon with a given color
     *
     * @param gc    GraphicsContext where to draw the polygon
     * @param color Color to be used
     */
    protected void drawSides(GraphicsContext gc, Color color) {
        gc.setStroke(color);
        drawSides(gc);
        gc.setStroke(Color.BLACK);
    }

    /**
     * Draws the figure with a dotted line
     *
     * @param gc GraphicsContext where to draw the polygon
     */
    protected void drawSidesDashed(GraphicsContext gc) {
        gc.setLineDashes(4 * zoom);
        drawSides(gc);
        gc.setLineDashes(0);
    }

    /**
     * Draws each side of the polygon with a given color
     *
     * @param gc    GraphicsContext where to draw the polygon
     * @param color Color to be used
     */
    protected void drawSidesDashed(GraphicsContext gc, Color color) {
        gc.setStroke(color);
        drawSidesDashed(gc);
        gc.setStroke(Color.BLACK);
    }

    /**
     * Fills the polygon with white color. To do so, it draws the same figure
     * many times, each with a smaller radius, until it becomes 0.
     *
     * @param gc GraphicsContext where the polygon is drawn
     */
    protected void fill(GraphicsContext gc) {
        int aux = radius;
        int aux2 = radius2;
        Color color = Color.WHITE;
        while (radius > 0 || radius2 > 0) {
            if (radius > 0)
                radius -= 1;
            if (radius2 > 0)
                radius2 -= 1;
            computeCorners();
            computeControlPoints();
            drawSides(gc, color);
        }
        //Set back the original attributes of the polygon
        this.radius = aux;
        this.radius2 = aux2;
        computeCorners();
        computeControlPoints();

    }

    /**
     * Underlines the label of this Polygon
     *
     * @param gc GraphicsContext where to draw
     */
    private void underlineLabel(GraphicsContext gc) {
        gc.setLineWidth(1 * zoom);
        gc.strokeLine(this.center.getX() - label.getLayoutBounds().getWidth() * zoom / 2, this.center.getY() + 10 * zoom,
                this.center.getX() + label.getLayoutBounds().getWidth() * zoom / 2 , this.center.getY() + 10 * zoom);
    }

    /**
     * Underlines the label of this Polygon with a dashed line
     *
     * @param gc GraphicsContext where to draw
     */
    private void underlineLabelDashed(GraphicsContext gc) {
        gc.setLineWidth(1 * zoom);
        gc.setLineDashes(3 * zoom);
        underlineLabel(gc);
        gc.setLineDashes(0);
    }

    protected void applyZoom() {
        computeRadius();
        this.radius = (int) (radius * zoom);
        this.radius2 = (int) (radius2 * zoom);
        setCenter(getCenter().getX() * zoom, getCenter().getY() * zoom);
        computeControlPoints();
        computeCorners();
        computeAnchorPoints();
        setAvailablePoints();
    }

    protected void removeZoom() {
        setCenter(getCenter().getX() / zoom, getCenter().getY() / zoom);
    }

    protected void computeAnchorPoints() {
        for (int i = 0; i < corners.length; i++) {
            anchorPoints[i] = corners[i];
        }
    }


    public Polygon clone() {
        Polygon obj = null;
        try {
            obj = (Polygon) super.clone();
            obj.label = new Text(label.getText());
            obj.corners = corners.clone();
            obj.controlPoints = controlPoints.clone();
            obj.anchorPoints = anchorPoints.clone();
            obj.center = center.clone();
            for (int i = 0; i < availableCorners.size(); i++)
                obj.availableCorners.set(i, availableCorners.get(i).clone());
        } catch (CloneNotSupportedException ex) {
            System.out.println("Can't clone Polygon");
        }
        return obj;
    }

    public void build() {
        computeRadius();
        computeControlPoints();
        computeCorners();
        computeAnchorPoints();
    }
}



