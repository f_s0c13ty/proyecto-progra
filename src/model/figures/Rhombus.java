package model.figures;

import utilities.CustomPoint2D;

/**
 * Rhombus class
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 0X.09.18 (Modified by: xxxx)
 */
public class Rhombus extends Polygon implements Cloneable {

    public Rhombus(String labelText, double centerX, double centerY) {
        super(labelText, 20);
        this.corners = new CustomPoint2D[4];
        this.anchorPoints = new CustomPoint2D[4];
        this.offset = 0;
        setCenter(centerX / zoom, centerY / zoom);
        build();
    }

}
