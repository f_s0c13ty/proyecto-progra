package model.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import utilities.CustomPoint2D;


public class Ellipse extends Polygon implements Cloneable{

    public Ellipse(String label, double centerX, double centerY) {
        super(label, 20);
        this.corners = new CustomPoint2D[24];
        this.anchorPoints = new CustomPoint2D[24];
        this.offset = 0;
        this.spaceBetweenDoubleLines = 3;
        setCenter(centerX / zoom, centerY / zoom);
        build();
    }


    @Override
    public void computeRadius() {
        super.computeRadius();
        //Define a minimum radius for any ellipse
        if(this.radius < 40) {
            this.radius = 40;
        }
        this.radius2 = 30;
    }

    @Override
    protected void drawSides(GraphicsContext gc) {
        gc.setLineWidth(2);
        gc.strokeArc(this.center.getX()-radius, this.center.getY()-radius2, 2*radius, 2*radius2, 0, 360, ArcType.OPEN);
    }








}


