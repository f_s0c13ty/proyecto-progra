package model.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.ArcType;
import utilities.CustomPoint2D;


public class Circle extends Polygon implements Cloneable {

    public Circle(String label, double centerX, double centerY) {
        super(label, 10);
        this.offset = 0;
        this.corners = new CustomPoint2D[24];
        this.anchorPoints = new CustomPoint2D[24];
        setCenter(centerX / zoom, centerY / zoom);
        build();
    }

    @Override
    public void drawSides(GraphicsContext gc) {
        gc.setLineWidth(2);
        gc.strokeArc(this.center.getX()-radius, this.center.getY()-radius, 2*radius, 2*radius, 0, 360, ArcType.OPEN);
    }


}
