package model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.figures.Circle;

import java.util.ArrayList;
import java.util.HashMap;

public class EntityInheritance implements Cloneable {

    EntityInheritanceType type;
    private Entity parent;
    private ArrayList<Entity> children;
    private Circle figure;

    public EntityInheritance(EntityInheritanceType type, double centerX, double centerY) {
        this.type = type;
        if (type == EntityInheritanceType.DISJOINT)
            figure = new Circle("D", centerX, centerY);
        if (type == EntityInheritanceType.OVERLAPPING)
            figure = new Circle("O", centerX, centerY);
        this.children = new ArrayList<>();
    }

    public EntityInheritance(){
        children = new ArrayList<>();
    }

    public EntityInheritanceType getType() {
        return this.type;
    }

    public void setType(EntityInheritanceType type) {
        this.type = type;
        if (type == EntityInheritanceType.DISJOINT)
            figure.setLabel("D");
        if (type == EntityInheritanceType.OVERLAPPING)
            figure.setLabel("O");
    }

    public Entity getParent() {
        return parent;
    }

    public void setParent(Entity parent) {
        this.parent = parent;
    }

    public ArrayList<Entity> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Entity> children) {
        this.children = children;
    }

    public void addChildren(Entity entity) {
        this.children.add(entity);
    }

    public void removeChildren(Entity entity) {
        for (Entity child : children) {
            if (child.equals(entity)) {
                child.setChild(false);
                children.remove(child);
                break;
            }
        }

        if (children.isEmpty())
            parent.removeInheritance();

    }

    public Circle getFigure() {
        return this.figure;
    }

    public void setFigure(Circle figure) {
        this.figure = figure;
    }

    public void setClonedChildren(ArrayList<Entity> entitiesCloned){
        ArrayList<Entity> aux = new ArrayList<>();
        children.forEach(child ->
        {

            for (Entity cloned : entitiesCloned) {
                if (child.getId() == cloned.getId()) {
                    aux.add(cloned);
                    break;
                }
            }
        });
        this.children = aux;
    }

    public void showOnCanvas(GraphicsContext gc, Color color) {
        this.figure.draw(gc, color);
        drawConnectingLines(gc);
    }

    private void drawConnectingLines(GraphicsContext gc) {
        ConnectingLine.draw(gc,parent,figure);
        if (!children.isEmpty())
            children.forEach((entity) -> ConnectingLine.draw(gc,this.figure,entity));
    }

    public EntityInheritance clone(Entity entityCloned){
        EntityInheritance obj = new EntityInheritance();
        obj.type = this.type;
        obj.setParent(entityCloned);
        obj.setFigure((Circle) this.figure.clone());
        obj.setChildren(children);
        return obj;
    }

}
