package controller;

import application.Main;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.*;
import model.figures.Polygon;
import utilities.CustomPoint2D;
import utilities.ExportationHandler;
import utilities.SceneHandler;
import utilities.SemanticVerifier;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * <code>MainWindowController</code> is a class that controls the actions of the
 * MainWindow.fxml
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 15.10.18 (Modified by: Edward Becerra) Show Control Points Key/Button Fix
 */
public class MainWindowController implements Initializable {

    @FXML
    private static GraphicsContext gc;

    @FXML
    private Label notificationLabel;

    @FXML
    private ScrollPane canvasScrollPane;

    // Canvas where all the figures will be drawn
    @FXML
    private Canvas canvas;

    @FXML
    private Label undoLabel;

    @FXML
    private Label zoomLabel;

    // Show/Hide Control Points
    @FXML
    private ToggleButton controlPointsButton;

    @FXML
    private ImageView iconShowControlPoints;

    /* --- Variables for Properties --- */
    @FXML
    private TextArea warningText;
    @FXML
    private Accordion accordion;
    @FXML
    private TitledPane propertiesTitlePane, warningTitlePane;
    @FXML
    private Text textFieldLabel, topPaneLabel, bottomPaneLabel, midPaneLabel, cardinalityLabelTop, cardinalityLabelBottom, doubleLineLabelTop, doubleLineLabelBottom;

    @FXML
    private VBox topVBox, bottomVBox, midVBox;

    @FXML
    private TextField textField;

    @FXML
    private ScrollPane topPane, bottomPane, midPane;

    @FXML
    private Button newAttributeButton, newAggregationButton, deleteButton;

    @FXML
    private RadioButton disjointSelect, overlappingSelect;

    final ToggleGroup inheritanceTypesGroup = new ToggleGroup();

    @FXML
    private CheckBox weakCheckbox, doubleLineCBTop, doubleLineCBBottom;

    @FXML
    private ComboBox topComboBox, bottomComboBox;

    /* --- Variables for Properties --- */

    private static boolean askedToDrawEntity, askedToDrawRelation, askedToDrawAttribute, askedToDrawAggregation, controlPointsShowed = false;

    private static Entity entitySelected;

    private static Relation relationSelected;

    private static Attribute attributeSelected;

    private EntityInheritance inheritanceSelected;

    private Aggregation aggregationSelected;

    private CustomPoint2D pointClicked;

    private ArrayList<Entity> selectedEntities = new ArrayList<>();

    private ArrayList<Attribute> selectedAttributes = new ArrayList<>();

    private ArrayList<Relation> selectedRelations = new ArrayList<>();

    private ArrayList<Aggregation> selectedAggregations = new ArrayList<>();

    // private int elementsSelecteds = 0;

    private Color selectedColor = Color.MEDIUMBLUE;

    public static double scaleZoom = 1;

    @FXML
    private MenuItem undoMenuItem;

    @FXML
    private MenuItem redoMenuItem;

    @FXML
    private MenuItem zoomInMenuItem;

    @FXML
    private MenuItem zoomOutMenuItem;

    // Minimum size values for the canvas
    private final int minCanvasWidth = 800, mindCanvasHeight = 650;

    private boolean isDragging = false;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.gc = canvas.getGraphicsContext2D();
        this.accordion.setExpandedPane(propertiesTitlePane);
        this.textField.textProperty().addListener((obs, oldText, newText) -> editName(newText));
        this.disjointSelect.setToggleGroup(inheritanceTypesGroup);
        this.overlappingSelect.setToggleGroup(inheritanceTypesGroup);
        this.zoomLabel.setText("Scale at: 1.0");
        this.topComboBox.getItems().addAll("1", "N");
        this.bottomComboBox.getItems().addAll("1", "N");
        this.warningText.setWrapText(true);

    }

    @FXML
    void keyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case ESCAPE:
                notificationLabel.setVisible(false);
                canvas.setOnMouseClicked(e -> selectedFigure(e));
                break;
            case DELETE:
                if (entitySelected != null)
                    Main.diagram.eraseEntity(entitySelected);
                else if (relationSelected != null)
                    Main.diagram.eraseRelation(relationSelected);
                else if (attributeSelected != null)
                    Main.diagram.eraseAttribute(attributeSelected);
                else if (inheritanceSelected != null)
                    Main.diagram.eraseInheritance(inheritanceSelected);
                else if (aggregationSelected != null)
                    Main.diagram.eraseAggregation(aggregationSelected);
                drawAllFigures();
                hideSideBar();
        }
        if (!event.isShiftDown() && event.getCode() == KeyCode.E)
            newEntityPressed();
        if (!event.isShiftDown() && event.getCode() == KeyCode.R)
            newRelationPressed();
        if (!event.isShiftDown() && event.getCode() == KeyCode.A && (entitySelected != null || (attributeSelected != null && attributeSelected.getAttributeType() == AttributeType.COMPOSITE) || relationSelected != null))
            newAttributePressed();
        if (!event.isShiftDown() && event.getCode() == KeyCode.S) {
            if (!this.controlPointsButton.isSelected())
                this.controlPointsButton.setSelected(true);
            else
                this.controlPointsButton.setSelected(false);
            controlPointsButtonAction();
        }
        if (!event.isShiftDown() && event.getCode() == KeyCode.G && relationSelected != null && !isInAggregation(relationSelected))
            newAggregationPressed();
        if (!event.isShiftDown() && event.getCode() == KeyCode.C)
            clearButtonPressed();
        if (event.isControlDown() && event.getCode() == KeyCode.PLUS)
            zoomInSelected();
        if (event.isControlDown() && event.getCode() == KeyCode.MINUS)
            zoomOutSelected();
        if ((event.isControlDown() && event.isShiftDown()) && event.getCode() == KeyCode.Z)
            redoSelected();
        else if (event.isControlDown() && event.getCode() == KeyCode.Z)
            undoSelected();
    }

    public static Attribute getAttributeSelected() {
        return attributeSelected;
    }

    /**
     * Exit from the application
     */
    @FXML
    void exit() {
        System.exit(0);
    }

    /**
     * Refreshes the figures drawn on the canvas
     */
    private void drawAllFigures() {
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        drawAllEntities();
        drawAllAttributes();
        drawAllRelations();
        drawAllAggregations();
        resizeCanvas();
    }

    /**
     * Draws all the entities on the canvas
     */
    private void drawAllEntities() {
        for (Entity entity : Main.diagram.getEntities()) {
            if ((entitySelected != null && entitySelected == entity) || (!selectedEntities.isEmpty() && selectedEntities.contains(entity)))
                entity.showOnCanvas(gc, selectedColor);
            else entity.showOnCanvas(gc, Color.BLACK);
            if (entity.getInheritance() != null) {
                entity.getInheritance().getFigure().setAvailablePoints();
                if (inheritanceSelected != null && inheritanceSelected == entity.getInheritance())
                    entity.getInheritance().showOnCanvas(gc, selectedColor);
                else {
                    entity.getInheritance().showOnCanvas(gc, Color.BLACK);
                }
                if (controlPointsButton.isSelected()) {
                    entity.getInheritance().getFigure().showCorners(gc);
                }
            }
            if (controlPointsButton.isSelected()) {
                entity.getPolygon().showCorners(gc);
            }
        }
    }

    /**
     * Draws all the relations on the canvas. First it draws all the polygons
     * and then all the connecting lines
     */
    private void drawAllRelations() {
        for (Relation relation : Main.diagram.getRelations()) {
            if ((relationSelected != null && relationSelected == relation) || (!selectedRelations.isEmpty() && selectedRelations.contains(relation)))
                relation.showFigureOnCanvas(gc, selectedColor);
            else
                relation.showFigureOnCanvas(gc, Color.BLACK);
            relation.showConnectingLinesOnCanvas(gc);
            if (controlPointsButton.isSelected()) {
                relation.getPolygon().showCorners(gc);
            }
        }
    }

    private void drawAllAttributes() {
        Main.diagram.getAttributes().forEach(attribute -> {
            if (attributeSelected != null && attributeSelected == attribute)
                attribute.showOnCanvas(gc, selectedColor);
            else
                attribute.showOnCanvas(gc, Color.BLACK);
            if (controlPointsButton.isSelected())
                attribute.getEllipse().showCorners(gc);
        });
    }

    private void drawAllAggregations() {
        for (Aggregation aggregation : Main.diagram.getAggregations()) {
            if (aggregationSelected != null && aggregationSelected == aggregation)
                aggregation.showOnCanvas(gc, selectedColor);
            else
                aggregation.showOnCanvas(gc, Color.BLACK);
            if (controlPointsButton.isSelected())
                aggregation.getPolygon().showCorners(gc);
        }
    }

    /**
     * Delete every element drawn in the canvas
     */
    @FXML
    void clearButtonPressed() {
        Main.diagram.clear();
        //undoMenuItem.setDisable(true);
        //redoMenuItem.setDisable(true);
        hideSideBar();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }




    /* ----------------   Control Points  ------------------------- */

    @FXML
    void controlPointsButtonAction() {
        if (!controlPointsShowed) { // Show the Control Points if they are not visible
            for (Entity entity : Main.diagram.getEntities()) {
                entity.getPolygon().showCorners(gc);
                if (entity.getInheritance() != null) {
                    entity.getInheritance().getFigure().showCorners(gc);
                }
            }
            for (Relation relation : Main.diagram.getRelations()) {
                relation.getPolygon().showCorners(gc);
            }
            for (Attribute attribute : Main.diagram.getAttributes()) {
                attribute.getEllipse().showCorners(gc);
            }
            for(Aggregation aggregation : Main.diagram.getAggregations()) {
                aggregation.getPolygon().showCorners(gc);
            }



            iconShowControlPoints.setImage(new Image("images/icons8-eye-48.png"));
            MainWindowController.controlPointsShowed = true;
        } else { // Hide the Control Points if they are visible
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            drawAllFigures();
            this.controlPointsButton.setText("Show Control Points");
            iconShowControlPoints.setImage(new Image("images/icons8-hide-48.png"));
            MainWindowController.controlPointsShowed = false;
        }
    }

    /* ---------------   End Control Points   ---------------------- */




    /*---------------------   Create Entity   ----------------------*/


    public static void setAskedToDrawEntity(boolean askedToDrawEntity) {
        MainWindowController.askedToDrawEntity = askedToDrawEntity;
    }

    /**
     * Opens a new window to fill with the information to create a new Entity
     */
    @FXML
    void newEntityPressed() {
        //Load pop-up
        SceneHandler.loadPopUp("New Entity", getClass().getResource("../view/NewEntityModal.fxml"), new CustomPoint2D(200, 200));
        // draw the entity if the user asked for it
        if (askedToDrawEntity) {
            MainWindowController.askedToDrawEntity = false;
            notificationLabel.setVisible(true);
            createAndShowEntity();
        }
    }

    private void createAndShowEntity() {
        canvas.setOnMouseClicked(e -> {
            Entity entity = new Entity(NewEntityModalController.getEntityName());
            //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
            //redoMenuItem.setDisable(true);
            //undoMenuItem.setDisable(false);
            Main.diagram.addEntity(entity);
            notificationLabel.setVisible(false);
            entity.setPolygon(e.getX(), e.getY());
            drawAllFigures();
            canvas.setOnMouseClicked(event1 -> {
                selectedFigure(event1);
            });
        });
        hideSideBar();
    }

    /*-------------------   End Create Entity   ---------------------*/




    /* -------------------   Create Relation   ----------------------- */

    public static void setAskedToDrawRelation(boolean askedToDrawRelation) {
        MainWindowController.askedToDrawRelation = askedToDrawRelation;
    }

    /**
     * Opens a new window to fill with the information to create a new Relation
     */
    @FXML
    void newRelationPressed() {
        SceneHandler.loadPopUp("New Relation", getClass().getResource("../view/NewRelationModal.fxml"), new CustomPoint2D(200, 200));
        if (askedToDrawRelation) {
            MainWindowController.askedToDrawRelation = false;
            notificationLabel.setVisible(true);
            createAndShowRelation();
        }
    }

    private void createAndShowRelation() {
        canvas.setOnMouseClicked(e -> {
            Relation relation = new Relation(NewRelationModalController.getRelationName(), NewRelationModalController.getRelatedEntities(), NewRelationModalController.getRelatedAggregations());
            //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
            //redoMenuItem.setDisable(true);
            Main.diagram.addRelation(relation);
            notificationLabel.setVisible(false);
            relation.setMyFigure((int) e.getX(), (int) e.getY());
            drawAllFigures();

            canvas.setOnMouseClicked(mouseEvent -> {
                selectedFigure(mouseEvent);
            });
        });
        hideSideBar();
    }

    /* ----------------   End Create Relation   --------------------- */


    /* ------------------   Figures Movement   -------------------- */

    @FXML
    public void onMousePressed(MouseEvent event) {
        if (searchFigure(event)) {
            //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
            //redoMenuItem.setDisable(true);
            isDragging = true;
            if (aggregationSelected != null) {
                pointClicked = new CustomPoint2D(event.getX(), event.getY());
                aggregationSelected.saveCurrentCenters();
                for (Aggregation aggregation : aggregationSelected.getAggregations()) {
                    aggregation.saveCurrentCenters();
                }
            }
        }
        drawAllFigures();
    }

    /**
     * searchFigure: Finds the figure selected on canvas
     * Sets all the selectable variables to null in order to check when the canvas has been checked
     * and not a figure
     *
     * @param event
     * @return true if a figure was press and false if not
     */
    public boolean searchFigure(MouseEvent event) {
        entitySelected = null;
        relationSelected = null;
        attributeSelected = null;
        inheritanceSelected = null;
        aggregationSelected = null;

        for (Entity entity : Main.diagram.getEntities()) {
            if (entity.getPolygon().doesContainsCoords(event.getX(), event.getY())) {
                entitySelected = entity;
                return true;
            }
            if (entity.getInheritance() != null && entity.getInheritance().getFigure().doesContainsCoords(event.getX(), event.getY())) {
                inheritanceSelected = entity.getInheritance();
                return true;
            }
        }
        for (Relation relation : Main.diagram.getRelations()) {
            if (relation.getPolygon().doesContainsCoords(event.getX(), event.getY())) {
                relationSelected = relation;
                return true;
            }
        }
        for (Attribute attribute : Main.diagram.getAttributes()) {
            if (attribute.getEllipse().doesContainsCoords(event.getX(), event.getY())) {
                attributeSelected = attribute;
                return true;
            }
        }
        for (Aggregation aggregation : Main.diagram.getAggregations()) {
            if (aggregation.getPolygon().clickOnBorders(event.getX(), event.getY())) {
                aggregationSelected = aggregation;
                return true;
            }
        }
        return false;
    }

    @FXML
    void onMouseReleased() {
        isDragging = false;
        entitySelected = null;
        relationSelected = null;
        attributeSelected = null;
        aggregationSelected = null;
    }

    /**
     * moveFigure: Moves the selected figure when the drag event has been released
     *
     * @param event
     */
    public void onMouseDragged(MouseEvent event) {
        if (isDragging) {
            if (entitySelected != null)
                entitySelected.getPolygon().setCenterToMove(event.getX(), event.getY());
            else if (relationSelected != null)
                relationSelected.getPolygon().setCenterToMove(event.getX(), event.getY());
            else if (attributeSelected != null)
                attributeSelected.getEllipse().setCenterToMove(event.getX(), event.getY());
            else if (inheritanceSelected != null)
                inheritanceSelected.getFigure().setCenterToMove(event.getX(), event.getY());
            else if (aggregationSelected != null) {
                CustomPoint2D mousePoint = new CustomPoint2D(event.getX(), event.getY());
                aggregationSelected.moveFigure(mousePoint.getX() - pointClicked.getX(), mousePoint.getY() - pointClicked.getY());
            }
            drawAllFigures();
            resizeCanvas();
        }
    }
    /* -----------------   End Figures Movement   --------------- */



    /* ------------------ Exportation ---------------------- */

    @FXML
    void handleExportAsImageAction() {
        exportAs("png");
    }

    @FXML
    void handleExportAsPDFAction() {
        exportAs("pdf");
    }

    @FXML
    void helpPressed() {
        SceneHandler.loadPopUp("Help", getClass().getResource("../view/HelpWindow.fxml"), new CustomPoint2D(200, 200));
    }

    private void exportAs(String choice) {
        Main.diagram.setHeight(Main.diagram.computeHeight());
        Main.diagram.setWidth(Main.diagram.computeWidth());
        int margins = 20;
        int width = Main.diagram.getWidth() + margins;
        int height = Main.diagram.getHeight() + margins;
        switch (choice) {
            case "pdf":
                ExportationHandler.exportAsPDF(canvas, width, height);
                break;
            case "png":
                ExportationHandler.exportAsPNG(canvas, width, height);
                break;
        }
    }

    /* ----------------   End Exportation  ------------------ */


    /******************* SIDE BAR FUNCTIONS *******************/

    /* -----------------   Selected Figures   --------------- */


    /**
     * selectedFigure: Check which figure was selected in order to send the info to the side bar
     *
     * @param event
     */
    private void selectedFigure(MouseEvent event) {
        hideSideBar();
        if (searchFigure(event)) {
            this.topVBox.getChildren().clear();
            this.bottomVBox.getChildren().clear();
            this.midVBox.getChildren().clear();
            this.deleteButton.setVisible(true);
            if (entitySelected != null) {
                putEntityOnSideBar();
                if (accordion.getExpandedPane() != propertiesTitlePane) {
                    accordion.setExpandedPane(propertiesTitlePane);
                }
            } else if (relationSelected != null) {
                putRelationOnSideBar();
                if (accordion.getExpandedPane() != propertiesTitlePane) {
                    accordion.setExpandedPane(propertiesTitlePane);
                }
            } else if (attributeSelected != null) {
                putAttributesOnSideBar();
                if (accordion.getExpandedPane() != propertiesTitlePane) {
                    accordion.setExpandedPane(propertiesTitlePane);
                }
            } else if (inheritanceSelected != null) {
                putInheritanceOnSideBar();
                if (accordion.getExpandedPane() != propertiesTitlePane) {
                    accordion.setExpandedPane(propertiesTitlePane);
                }
            } else if (aggregationSelected != null)
                putAggregationOnSideBar();
            if (accordion.getExpandedPane() != propertiesTitlePane) {
                accordion.setExpandedPane(propertiesTitlePane);
            }
        }
    }

    private void hideSideBar() {
        entitySelected = null;
        relationSelected = null;
        attributeSelected = null;

        this.textFieldLabel.setVisible(false);
        this.textField.setVisible(false);
        this.weakCheckbox.setVisible(false);
        this.weakCheckbox.setSelected(false);
        this.doubleLineCBTop.setVisible(false);
        this.doubleLineCBBottom.setVisible(false);
        this.doubleLineLabelTop.setVisible(false);
        this.doubleLineLabelBottom.setVisible(false);
        this.disjointSelect.setVisible(false);
        this.overlappingSelect.setVisible(false);
        this.cardinalityLabelTop.setVisible(false);
        this.cardinalityLabelBottom.setVisible(false);
        this.topComboBox.setVisible(false);
        this.bottomComboBox.setVisible(false);
        this.topPane.setVisible(false);
        this.topPaneLabel.setVisible(false);
        this.midPane.setVisible(false);
        this.midPaneLabel.setVisible(false);
        this.bottomPane.setVisible(false);
        this.bottomPaneLabel.setVisible(false);
        this.newAttributeButton.setVisible(false);
        this.newAggregationButton.setVisible(false);
        this.deleteButton.setVisible(false);
    }


    /**
     * getNewCheckBox: Sets a new clickeable checkbox defining it's state and it's action when clicked
     *
     * @param relation
     * @return new Checkbox
     */
    private CheckBox getNewCheckBox(Relation relation) {
        CheckBox newCheckBox = new CheckBox(relation.getName());
        int[] figureIndex = new int[2];
        if (attributeSelected != null) {
            if (attributeSelected.getRelationConnected() != null && attributeSelected.getRelationConnected().equals(relation))
                newCheckBox.setSelected(true);
            figureIndex[0] = 2;
            figureIndex[1] = Main.diagram.getAttributes().indexOf(attributeSelected);
        }
        newCheckBox.selectedProperty().addListener((obs, oldValue, newValue) -> editRelationConnections(oldValue, newValue, relation, figureIndex));
        return newCheckBox;
    }

    private CheckBox getNewCheckBox(Entity entity) {
        CheckBox newCheckBox = new CheckBox(entity.getName());
        int[] figureIndex = new int[2];
        if (entitySelected != null) {
            if (entitySelected.getInheritance() != null) {
                if (entitySelected.getInheritance().getChildren().contains(entity))
                    newCheckBox.setSelected(true);
                figureIndex[0] = 0;
                figureIndex[1] = Main.diagram.getEntities().indexOf(entitySelected);
            }
        } else if (relationSelected != null) {
            if (relationSelected.getEntities().containsValue(entity))
                newCheckBox.setSelected(true);
            figureIndex[0] = 1;
            figureIndex[1] = Main.diagram.getRelations().indexOf(relationSelected);
        } else {
            if (attributeSelected.getEntityConnected() != null && attributeSelected.getEntityConnected().equals(entity))
                newCheckBox.setSelected(true);
            figureIndex[0] = 2;
            figureIndex[1] = Main.diagram.getAttributes().indexOf(attributeSelected);
        }
        newCheckBox.selectedProperty().addListener((obs, oldValue, newValue) -> editEntityConnections(oldValue, newValue, entity, figureIndex));
        return newCheckBox;
    }

    private CheckBox getNewCheckBox(Attribute attribute) {
        CheckBox newCheckBox = new CheckBox(attribute.getName());
        int[] figureIndex = new int[2];
        if (attributeSelected != null) {
            if (attributeSelected.getAttributes().contains(attribute))
                newCheckBox.setSelected(true);
            figureIndex[0] = 2;
            figureIndex[1] = Main.diagram.getAttributes().indexOf(attributeSelected);
        }
        newCheckBox.selectedProperty().addListener((obs, oldValue, newValue) -> editAttributeConnections(oldValue, newValue, attribute, figureIndex));
        return newCheckBox;
    }

    private CheckBox getNewCheckBox(Aggregation aggregation) {
        CheckBox checkBox = new CheckBox(aggregation.getName());
        if (relationSelected.getAggregations().containsValue(aggregation))
            checkBox.setSelected(true);
        int relationIndex = Main.diagram.getRelations().indexOf(relationSelected);

        checkBox.selectedProperty().addListener((obs, oldValue, newValue) -> editAggregationConnections(oldValue, newValue, aggregation, relationIndex));
        return checkBox;
    }

    private void editAggregationConnections(boolean oldValue, boolean newValue, Aggregation aggregation, int relationIndex){
        if(!oldValue && newValue){
            if (!Main.diagram.getRelations().get(relationIndex).getType().equals(RelationType.BINARY))
                    Main.diagram.getRelations().get(relationIndex).addAggregation(aggregation);
        }else if(oldValue && !newValue)
            Main.diagram.getRelations().get(relationIndex).removeAggregation(aggregation);
        
        drawAllFigures();

        hideSideBar();
        relationSelected = Main.diagram.getRelations().get(relationIndex);
        putRelationOnSideBar();
    }

    /**
     * putEntityOnSideBar: Sets the name of an entity, the relations and attributes that are connected to it and
     * the ones available.
     */
    private void putEntityOnSideBar() {
        this.textFieldLabel.setVisible(true);
        this.textFieldLabel.setText("Entity:");
        this.textField.setVisible(true);
        this.textField.setText(entitySelected.getName());
        this.weakCheckbox.setVisible(true);
        this.weakCheckbox.setSelected(entitySelected.isWeak());
        this.weakCheckbox.selectedProperty().addListener((obs, oldValue, newValue) -> setWeakFigure(oldValue, newValue, entitySelected, relationSelected));


        //Set Entity's children at the mid VBox
        if (!entitySelected.isChild()) {
            this.topPane.setVisible(true);
            this.topPaneLabel.setText("My Children:");
            this.topPaneLabel.setVisible(true);

            Main.diagram.getEntities().forEach(entity -> {
                if (!entity.equals(entitySelected) && entity.getInheritance() == null) {
                    CheckBox newCheckBox = getNewCheckBox(entity);
                    topVBox.getChildren().add(newCheckBox);
                }
            });
        }
        this.newAttributeButton.setVisible(true);

        this.deleteButton.setOnAction(action -> {
            Main.diagram.eraseEntity(entitySelected);
            drawAllFigures();
            hideSideBar();
        });
    }

    /**
     * putRelationOnSideBar: Sets the name of a relation, the entities and attributes that are connected to it and
     * the ones available.
     */
    private void putRelationOnSideBar() {
        this.midPane.setVisible(false);
        this.midPaneLabel.setVisible(false);

        this.textFieldLabel.setVisible(true);
        this.textFieldLabel.setText("Relation:");
        this.textField.setVisible(true);
        this.textField.setText(relationSelected.getName());
        this.weakCheckbox.setVisible(true);
        this.weakCheckbox.setSelected(relationSelected.isWeak());
        this.weakCheckbox.selectedProperty().addListener((obs, oldValue, newValue) -> setWeakFigure(oldValue, newValue, entitySelected, relationSelected));

        //Set Entities at the top VBox
        this.topPane.setVisible(true);
        this.topPaneLabel.setText("Entities:");
        this.topPaneLabel.setVisible(true);
        topVBox.getChildren().clear();
        if(relationSelected.getType().equals(RelationType.BINARY)){
            Main.diagram.getEntities().forEach(entity -> {
                if(relationSelected.getEntities().containsValue(entity)){
                    CheckBox newCheckBox = getNewCheckBox(entity);
                    topVBox.getChildren().add(newCheckBox);
                }
            });

            Main.diagram.getAggregations().forEach(aggregation -> {
                if(relationSelected.getAggregations().containsValue(aggregation)){
                    CheckBox newCheckBox = getNewCheckBox(aggregation);
                    topVBox.getChildren().add(newCheckBox);
                }
            });

        }else{
            Main.diagram.getEntities().forEach(entity -> {
                CheckBox newCheckBox = getNewCheckBox(entity);
                topVBox.getChildren().add(newCheckBox);
            });

            Main.diagram.getAggregations().forEach(aggregation -> {
                if (!aggregation.getRelations().contains(relationSelected)) {
                    CheckBox newCheckBox = getNewCheckBox(aggregation);
                    topVBox.getChildren().add(newCheckBox);
                }
            });

        }



        this.midPaneLabel.setText("Cardinalities");
        this.midPaneLabel.setVisible(true);
        if (relationSelected.getEntities().size() == 2 || relationSelected.getAggregations().size() == 2) {

            if (!relationSelected.getEntities().isEmpty())
                this.cardinalityLabelTop.setText(relationSelected.getEntity(0).getName().length() > 6 ? relationSelected.getEntity(0).getName().substring(0, 6) + "..." : relationSelected.getEntity(0).getName());
            else if (!relationSelected.getAggregations().isEmpty())
                this.cardinalityLabelTop.setText(relationSelected.getAggregation(0).getName().length() > 6 ? relationSelected.getAggregation(0).getName().substring(0, 6) + "..." : relationSelected.getAggregation(0).getName());

            if (relationSelected.getEntities().size() > 1 && relationSelected.getEntity(1) != null) {
                this.cardinalityLabelBottom.setText(relationSelected.getEntity(1).getName().length() > 6 ? relationSelected.getEntity(1).getName().substring(0, 6) + "..." : relationSelected.getEntity(1).getName());
            }
            else if (!relationSelected.getAggregations().isEmpty() && relationSelected.getAggregation(1) != null) {
                this.cardinalityLabelBottom.setText(relationSelected.getAggregation(1).getName().length() > 6 ? relationSelected.getAggregation(1).getName().substring(0, 6) + "..." : relationSelected.getAggregation(1).getName());
            }

        } else if (relationSelected.getEntities().size() == 1 && relationSelected.getAggregations().size() == 1) {
            this.cardinalityLabelTop.setText(relationSelected.getEntity(0).getName().length() > 6 ? relationSelected.getEntity(0).getName().substring(0,6) + "..." : relationSelected.getEntity(0).getName());
            this.cardinalityLabelBottom.setText(relationSelected.getAggregation(0).getName().length() > 6 ? relationSelected.getAggregation(0).getName().substring(0, 6) + "..." : relationSelected.getAggregation(0).getName());
        }
        this.cardinalityLabelTop.setVisible(true);
        this.cardinalityLabelBottom.setVisible(true);
        this.topComboBox.setVisible(true);
        this.topComboBox.getSelectionModel().select("" + relationSelected.getCardinality()[0]);
        this.bottomComboBox.setVisible(true);
        this.bottomComboBox.getSelectionModel().select("" + relationSelected.getCardinality()[1]);
        this.newAttributeButton.setVisible(true);
        if (relationSelected != null && !isInAggregation(relationSelected))
            this.newAggregationButton.setVisible(true);
        this.topComboBox.valueProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) || !relationSelected.getCardinality()[0].equals(newValue)) {
                relationSelected.setCardinality(newValue, 0);
                drawAllFigures();
            }
        });

        this.bottomComboBox.valueProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) || !relationSelected.getCardinality()[1].equals(newValue)) {
                relationSelected.setCardinality(newValue, 1);
                drawAllFigures();
            }
        });

        this.bottomPaneLabel.setText("Participation");
        this.bottomPaneLabel.setVisible(true);


        if (relationSelected.getEntities().size() == 2 || relationSelected.getAggregations().size() == 2) {

            if (!relationSelected.getEntities().isEmpty())
                this.doubleLineLabelTop.setText(relationSelected.getEntity(0).getName().length() > 6 ? relationSelected.getEntity(0).getName().substring(0, 6) + "..." : relationSelected.getEntity(0).getName());
            else if (!relationSelected.getAggregations().isEmpty())
                this.doubleLineLabelTop.setText(relationSelected.getAggregation(0).getName().length() > 6 ? relationSelected.getAggregation(0).getName().substring(0, 6) + "..." : relationSelected.getAggregation(0).getName());

            if (relationSelected.getEntities().size() > 1 && relationSelected.getEntity(1) != null)
                this.doubleLineLabelBottom.setText(relationSelected.getEntity(1).getName().length() > 6 ? relationSelected.getEntity(1).getName().substring(0, 6) + "..." : relationSelected.getEntity(1).getName());
            else if (!relationSelected.getAggregations().isEmpty())
                this.doubleLineLabelBottom.setText(relationSelected.getAggregation(0).getName().length() > 6 ? relationSelected.getAggregation(0).getName().substring(0, 6) + "..." : relationSelected.getAggregation(0).getName());

        } else if (relationSelected.getEntities().size() == 1 && relationSelected.getAggregations().size() == 1) {
            this.doubleLineLabelTop.setText(relationSelected.getEntity(0).getName().length() > 6 ? relationSelected.getEntity(0).getName().substring(0, 6) : relationSelected.getEntity(0).getName());
            this.doubleLineLabelBottom.setText(relationSelected.getAggregation(0).getName().length() > 6 ? relationSelected.getAggregation(0).getName().substring(0, 6) : relationSelected.getAggregation(0).getName());
        }
        this.doubleLineLabelTop.setVisible(true);
        this.doubleLineCBTop.setVisible(true);
        this.doubleLineCBTop.setSelected(relationSelected.getDoubleLined()[0]);
        this.doubleLineCBTop.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (relationSelected.getDoubleLined()[0] != newValue) {
                relationSelected.setDoubleLined(newValue, 0);
            }
            drawAllFigures();
        });
        this.doubleLineLabelBottom.setVisible(true);
        this.doubleLineCBBottom.setVisible(true);
        this.doubleLineCBBottom.setSelected(relationSelected.getDoubleLined()[1]);
        this.doubleLineCBBottom.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (relationSelected.getDoubleLined()[1] != newValue) {
                relationSelected.setDoubleLined(newValue, 1);
            }
            drawAllFigures();
        });

        this.deleteButton.setOnAction(action -> {
            Main.diagram.eraseRelation(relationSelected);
            drawAllFigures();
            hideSideBar();
        });
    }

    /**
     * putAttributesOnSideBar: Sets the name of an attribute, also the entities and relations connected to it
     * and the available ones
     */
    private void putAttributesOnSideBar() {
        this.weakCheckbox.setVisible(false);

        this.textFieldLabel.setVisible(true);
        this.textFieldLabel.setText("Attribute:");
        this.textField.setVisible(true);
        this.textField.setText(attributeSelected.getName());

        //Set Entities at the top VBox
        this.topPane.setVisible(true);
        this.topPaneLabel.setText("Entities:");
        this.topPaneLabel.setVisible(true);
        topVBox.getChildren().clear();
        Main.diagram.getEntities().forEach(entity -> {
            CheckBox newCheckBox = getNewCheckBox(entity);
            topVBox.getChildren().add(newCheckBox);
        });

        //Set Relations at the bottom VBox
        this.midPane.setVisible(true);
        this.midPaneLabel.setText("Relation:");
        this.midPaneLabel.setVisible(true);
        midVBox.getChildren().clear();
        Main.diagram.getRelations().forEach((relation) -> {
            CheckBox newCheckBox = getNewCheckBox(relation);
            midVBox.getChildren().add(newCheckBox);
        });


        //Set Attributes if it is an composite attribute
        if (attributeSelected.getAttributeType().equals(AttributeType.COMPOSITE)) {
            this.bottomPane.setVisible(true);
            this.bottomPaneLabel.setText("Attributes:");
            this.newAttributeButton.setVisible(true);
            this.bottomPaneLabel.setVisible(true);
            bottomVBox.getChildren().clear();
            Main.diagram.getAttributes().forEach(attribute -> {
                if (!attribute.equals(attributeSelected) && attribute.getAttributeType().equals(AttributeType.GENERIC)) {
                    CheckBox newCheckBox = getNewCheckBox(attribute);
                    bottomVBox.getChildren().add(newCheckBox);
                }
            });
        } else {
            this.bottomPane.setVisible(false);
            this.bottomPaneLabel.setVisible(false);
            this.newAttributeButton.setVisible(false);
        }

        this.deleteButton.setOnAction(action -> {
            Main.diagram.eraseAttribute(attributeSelected);
            drawAllFigures();
            hideSideBar();
        });
    }

    private void putInheritanceOnSideBar() {
        this.textFieldLabel.setVisible(true);
        this.textFieldLabel.setText("Inheritance:");
        if (inheritanceSelected.getType() == EntityInheritanceType.DISJOINT)
            disjointSelect.setSelected(true);
        else
            overlappingSelect.setSelected(true);
        this.disjointSelect.setVisible(true);
        this.overlappingSelect.setVisible(true);
        inheritanceTypesGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle oldToggle, Toggle newToggle) {
                //Main.saveDiagramState();
                if (inheritanceTypesGroup.getSelectedToggle().equals(disjointSelect))
                    inheritanceSelected.setType(EntityInheritanceType.DISJOINT);
                else
                    inheritanceSelected.setType(EntityInheritanceType.OVERLAPPING);
                drawAllFigures();
            }
        });

        this.deleteButton.setOnAction(action -> {
            Main.diagram.eraseInheritance(inheritanceSelected);
            drawAllFigures();
            hideSideBar();
        });
    }

    private void putAggregationOnSideBar() {
        this.textFieldLabel.setVisible(true);
        this.textFieldLabel.setText("Aggregation:");
        this.textField.setVisible(true);
        this.textField.setText(aggregationSelected.getName());
        this.deleteButton.setOnAction(action -> {
            Main.diagram.eraseAggregation(aggregationSelected);
            drawAllFigures();
            hideSideBar();
        });
    }

    /* -----------------   End Selected Figures   --------------- */


    /* -----------------   Edit Figures   --------------- */

    /**
     * editName: Edits dynamically the label of a figure at the front end and change the name of the class on the back end
     *
     * @param name
     */
    private void editName(String name) {
        name = name.isEmpty() ? "no name" : name;
        if (entitySelected != null && !entitySelected.getName().equals(name)) {
            if (!SemanticVerifier.entityNameHasBeenTaken(name)) {
                //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
                //redoMenuItem.setDisable(true);
                entitySelected.setName(name);
                entitySelected.getPolygon().setLabel(name);
            }
        } else if (relationSelected != null && !relationSelected.getName().equals(name)) {
            //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
            //redoMenuItem.setDisable(true);
            relationSelected.setName(name);
            relationSelected.getPolygon().setLabel(name);
        } else if (attributeSelected != null && !attributeSelected.getName().equals(name)) {
            //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
            //redoMenuItem.setDisable(true);
            attributeSelected.setName(name);
            attributeSelected.getEllipse().setLabel(name);
        } else if (aggregationSelected != null && !aggregationSelected.getName().equals(name)) {
            aggregationSelected.setName(name);
            aggregationSelected.getPolygon().setLabel(name);
        }
        drawAllFigures();
    }

    /**
     * editEntityConnections: Edit connection from a Relation, Attributes and Entity to an Entity
     *
     * @param oldValue
     * @param newValue
     * @param entity
     * @param figureIndex
     */
    private void editEntityConnections(boolean oldValue, boolean newValue, Entity entity, int[] figureIndex) {
        //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
        //redoMenuItem.setDisable(true);
        if (figureIndex[0] == 0) {
            if (!oldValue && newValue) {
                // For new values
                if (entitySelected.getInheritance() == null) {
                    // Create a new object EntityInheritance
                    CustomPoint2D midPoint = entitySelected.getPolygon().getCenter().midpoint(entity.getPolygon().getCenter());
                    entitySelected.setInheritance(EntityInheritanceType.OVERLAPPING, midPoint.getX(), midPoint.getY());
                    entitySelected.getInheritance().setParent(entitySelected);
                    entitySelected.getInheritance().addChildren(entity);
                    entity.setChild(true);
                } else if (!entitySelected.getInheritance().getChildren().isEmpty()) {
                    entitySelected.getInheritance().addChildren(entity);
                    entity.setChild(true);
                }
            } else {
                // If the value already exists in the list and is deselected
                entitySelected.getInheritance().removeChildren(entity);
                entity.setChild(false);
            }
        } else if (figureIndex[0] == 1) {
            if (!oldValue && newValue) {
                //From here we limit the amount of entity connected to an relation
                if (!Main.diagram.getRelations().get(figureIndex[1]).getType().equals(RelationType.BINARY)) {
                    Main.diagram.getRelations().get(figureIndex[1]).addEntity(entity);
                }
            } else if (oldValue && !newValue) {
                Main.diagram.getRelations().get(figureIndex[1]).removeEntity(entity);
            }

            hideSideBar();
            relationSelected = Main.diagram.getRelations().get(figureIndex[1]);
            putRelationOnSideBar();

        } else {
            if (!oldValue && newValue) {
                Main.diagram.getAttributes().get(figureIndex[1]).setEntityConnected(null);
                Main.diagram.getAttributes().get(figureIndex[1]).setRelationConnected(null);
                Main.diagram.getAttributes().get(figureIndex[1]).setEntityConnected(entity);

                for (Attribute attribute : Main.diagram.getAttributes()) {
                    if (attribute.getAttributeType().equals(AttributeType.COMPOSITE) && attribute.getAttributes().contains(Main.diagram.getAttributes().get(figureIndex[1]))) {
                        attribute.getAttributes().remove(Main.diagram.getAttributes().get(figureIndex[1]));
                        break;
                    }
                }
            } else {
                Main.diagram.getAttributes().get(figureIndex[1]).setEntityConnected(null);
            }
            hideSideBar();
            attributeSelected = Main.diagram.getAttributes().get(figureIndex[1]);
            putAttributesOnSideBar();
        }
        drawAllFigures();

    }


    /**
     * editRelationConnections: Edit connection from Attributes to a Relation
     *
     * @param oldValue
     * @param newValue
     * @param relation
     * @param figureIndex
     */
    private void editRelationConnections(boolean oldValue, boolean newValue, Relation relation, int[] figureIndex) {
        //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
        //redoMenuItem.setDisable(true);
        if (figureIndex[0] == 2) {
            if (!oldValue && newValue) {
                Main.diagram.getAttributes().get(figureIndex[1]).setEntityConnected(null);
                Main.diagram.getAttributes().get(figureIndex[1]).setRelationConnected(null);
                Main.diagram.getAttributes().get(figureIndex[1]).setRelationConnected(relation);

                for (Attribute attribute : Main.diagram.getAttributes()) {
                    if (attribute.getAttributeType().equals(AttributeType.COMPOSITE) && attribute.getAttributes().contains(Main.diagram.getAttributes().get(figureIndex[1]))) {
                        attribute.getAttributes().remove(Main.diagram.getAttributes().get(figureIndex[1]));
                        break;
                    }
                }
            } else {
                Main.diagram.getAttributes().get(figureIndex[1]).setRelationConnected(null);
            }

            hideSideBar();
            attributeSelected = Main.diagram.getAttributes().get(figureIndex[1]);
            putAttributesOnSideBar();
        }
        drawAllFigures();
    }

    /**
     * editAttributeConnections: Edit connections from attributes (if Composite) to attributes
     *
     * @param oldValue
     * @param newValue
     * @param attribute
     * @param figureIndex
     */
    private void editAttributeConnections(boolean oldValue, boolean newValue, Attribute attribute, int[] figureIndex) {
        //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
        //redoMenuItem.setDisable(true);
        if (figureIndex[0] == 2) {
            if (attribute.getEntityConnected() != null) {
                attribute.setEntityConnected(null);
            } else if (attribute.getRelationConnected() != null) {
                attribute.setRelationConnected(null);
            }
            if (!oldValue && newValue)
                Main.diagram.getAttributes().get(figureIndex[1]).addAttributes(attribute);
            else
                Main.diagram.getAttributes().get(figureIndex[1]).removeAttribute(attribute);
        }
        drawAllFigures();
    }

    private void setWeakFigure(boolean oldValue, boolean newValue, Entity entity, Relation relation) {
        if (oldValue != newValue) {
            if (entity != null && entity.isWeak() != newValue) {
                //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
                //redoMenuItem.setDisable(true);
                entity.setWeak(newValue);
            } else if (relation != null && relation.isWeak() != newValue) {
                //undoLabel.setText("Undo Steps: "+Main.saveDiagramState());
                //redoMenuItem.setDisable(true);
                relation.setWeak(newValue);
            }
            drawAllFigures();
        }
    }

    /* -----------------   End Edit Figures   --------------- */


    private void resizeCanvas() {
        int maxWidth = Main.diagram.computeWidth();
        int maxHeight = Main.diagram.computeHeight();
        if (maxWidth > minCanvasWidth)
            canvas.setWidth(maxWidth + 30);
        else
            canvas.setWidth(minCanvasWidth);

        if (maxHeight > mindCanvasHeight)
            canvas.setHeight(maxHeight + 30);
        else
            canvas.setHeight(mindCanvasHeight);
    }

    /* -------------------   Zoom   ----------------------- */

    @FXML
    private void zoomInSelected() {
        double zoomFactor = 0.2;
        if (scaleZoom < 2) {
            zoomAction(zoomFactor);
            if (scaleZoom == 2) {
                zoomInMenuItem.setDisable(true);
                zoomLabel.setText("Scale at: Max");
            }
        }
        zoomOutMenuItem.setDisable(false);
        resizeCanvas();
        drawAllFigures();
    }

    @FXML
    private void zoomOutSelected() {
        double zoomFactor = -0.2;
        if (scaleZoom > 0.4) {
            zoomAction(zoomFactor);
            if (scaleZoom == 0.4) {
                zoomOutMenuItem.setDisable(true);
                zoomLabel.setText("Scale at: Min");
            }
        }
        zoomInMenuItem.setDisable(false);
        resizeCanvas();
        drawAllFigures();
    }

    /**
     * Draws every figure on the canvas adding a zoom factor to the
     * current zoom
     *
     * @param zoomFactor the zoom variation
     */
    private void zoomAction(double zoomFactor) {
        scaleZoom += zoomFactor;
        scaleZoom = (double) Math.round(scaleZoom * 100) / 100;
        zoomLabel.setText(("Scale at: " + scaleZoom));
        Polygon.zoom = scaleZoom;
        drawAllFigures();
        resizeCanvas();

    }



    /* -------------------   End Zoom --------------------- */


    /* -------------------   Undo/Redo   ----------------------- */

    @FXML
    private void undoSelected() {
//        redoMenuItem.setDisable(false);
//        hideSideBar();
//        undoLabel.setText("Undo Steps: "+Main.undo());
//        Polygon.zoom = scaleZoom;
//        drawAllFigures();
//        if (Main.stackUndo.isEmpty())
//            //undoMenuItem.setDisable(true);
    }

    @FXML
    private void redoSelected() {
//        undoMenuItem.setDisable(false);
//        hideSideBar();
//        undoLabel.setText("Undo Steps: "+Main.redo());
//        Polygon.zoom = scaleZoom;
//        drawAllFigures();
//        if (Main.stackRedo.isEmpty())
//            //redoMenuItem.setDisable(true);
    }

    /* ------------------- End Undo/Redo  --------------------- */




    /* ---------------   Create Attribute   ---------------- */

    public void newAttributePressed() {
        SceneHandler.loadPopUp("New Attribute", getClass().getResource("../view/NewAttributeModal.fxml"), new CustomPoint2D(200, 200));
        if (askedToDrawAttribute) {
            askedToDrawAttribute = false;
            notificationLabel.setVisible(true);
            createAndShowAttribute();
            drawAllFigures();
            drawAllFigures();
        }
    }

    public static void setAskedToDrawAttribute(boolean askedToDraw) {
        askedToDrawAttribute = askedToDraw;
    }

    private void createAndShowAttribute() {
        Entity entity = entitySelected;
        Relation relation = relationSelected;
        Attribute att = attributeSelected;
        canvas.setOnMouseClicked(e -> {
            Attribute attribute = new Attribute(NewAttributeModalController.getAttributeName(), NewAttributeModalController.getAttributeType());
            if (entity != null)
                attribute.setEntityConnected(entity);
            if (relation != null)
                attribute.setRelationConnected(relation);
            if (att != null)
                att.addAttributes(attribute);
                attribute.setCompositeAttribute(att);
            //undoLabel.setText("Undo Steps: " + Main.saveDiagramState());
            //redoMenuItem.setDisable(true);
            Main.diagram.addAttribute(attribute);
            notificationLabel.setVisible(false);
            attribute.setMyFigure(e.getX(), e.getY());
            for (Aggregation aggregation : Main.diagram.getAggregations()) {
                if ((entity != null && aggregation.getEntities().contains(entity)) || (relation != null && aggregation.getRelations().contains(relation)) || (att != null && aggregation.getAttributes().contains(att))) {
                    aggregation.addAttribute(attribute);
                }
            }
            attributeSelected = attribute;
            drawAllAttributes();
            drawAllFigures();
            canvas.setOnMouseClicked(event1 -> selectedFigure(event1));
        });
        hideSideBar();
    }

    /* --------------  End Create Attribute   --------------- */



    /* ---------------- Create Aggregation ------------------ */

    public static void setAskedToDrawAggregation(boolean bool) {
        askedToDrawAggregation = bool;
    }

    private boolean isInAggregation(Relation relation) {
        for (Aggregation aggregation : Main.diagram.getAggregations()) {
            if (aggregation.getRelations().contains(relation))
                return true;
        }
        return false;
    }

    @FXML
    private void newAggregationPressed() {
        SceneHandler.loadPopUp("New Aggregation", getClass().getResource("../view/NewAggregationModal.fxml"), new CustomPoint2D(200, 200));
        if (askedToDrawAggregation == true && relationSelected != null) {
            Aggregation aggregation = new Aggregation(NewAggregationModalController.getAggregationName(), relationSelected);
            Main.diagram.addAggregation(aggregation);
            drawAllFigures();
            newAggregationButton.setVisible(false);
        }
        askedToDrawAggregation = false;
    }

    /* -------------- End Create Aggregation ---------------- */


    /* -------------- Semantic Verification -----------------*/
    @FXML
    private void verifyOnClick() {
        SemanticVerifier.warningContent = new StringBuilder();
        SemanticVerifier.verifyEveryWeakEntityHasPartialKey();
        SemanticVerifier.verifyWeakEntitiesRelation();
        SemanticVerifier.verifyWeakEntitiesRelation();
        SemanticVerifier.attributeChildNameAvailable();
        SemanticVerifier.verifyRelationsAndAttributesNotConnected();
        showValidationResults();
        accordion.setExpandedPane(warningTitlePane);
    }

    /**
     * Shows the results of the semantic verification on the warnings tab
     */
    private void showValidationResults() {
        warningText.setText(SemanticVerifier.warningContent.toString());
        if (warningText.getText().equals("")) {
            warningText.setText("No semantic errors found");
        }
    }

    /* -------------- End Semantic Verification -----------------*/
}
