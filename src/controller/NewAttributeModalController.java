package controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.Attribute;
import model.AttributeType;

import java.util.concurrent.RecursiveTask;

public class NewAttributeModalController {

    @FXML
    private TextField nameTextField;

    @FXML
    private ComboBox<String> comboBox;

    private static String comboBoxOption;

    // Button to cancel the creation of the current Entity
    @FXML
    private Button cancelButton;

    // Button to confirm the attributes of the Entity and create it
    @FXML
    private Button confirmButton;

    @FXML
    private static String name;

    public static String getAttributeName() {
        return name;
    }

    public static AttributeType getAttributeType(){return AttributeType.valueOf(comboBoxOption);}

    /**
     * Close the current modal and cancel the current operation
     */
    @FXML
    void cancelButtonPressed() {
        closeWindow();
    }

    /**
     * Get all the information needed to create the Entity and create it
     */
    @FXML
    void confirmButtonPressed() {
        name = nameTextField.getText();
        MainWindowController.setAskedToDrawAttribute(true);
        comboBoxOption = getComboBoxOption();
        Stage stage = (Stage) confirmButton.getScene().getWindow();
        stage.close();
    }




    @FXML
    void keyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case ENTER:
                confirmButtonPressed();
                break;
            case ESCAPE:
                closeWindow();
        }
    }

    private void closeWindow() {
        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    public void initialize() {
        comboBox.getItems().removeAll(comboBox.getItems());
        if (MainWindowController.getAttributeSelected() != null && MainWindowController.getAttributeSelected().getAttributeType() == AttributeType.COMPOSITE)
            comboBox.getItems().addAll("Generic");
        else
            comboBox.getItems().addAll("Generic", "Key", "Partial Key", "Multivalued", "Composite", "Derivative");
        comboBox.getSelectionModel().select("Generic");
    }

    private String getComboBoxOption() {
        String value = comboBox.getValue().toUpperCase();
        if(value.equals("PARTIAL KEY"))
            value = "PARTIAL_KEY";
        return value;

    }

}
