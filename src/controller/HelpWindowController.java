package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class HelpWindowController {

    @FXML
    private Button acceptButton;

    @FXML
    void acceptButtonPressed(ActionEvent event) {
        closeWindow();
    }

    @FXML
    void keyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case ENTER:
            case ESCAPE:
                closeWindow();
        }
    }

    private void closeWindow() {
        ((Stage) acceptButton.getScene().getWindow()).close();
    }

}
