package controller;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Aggregation;
import model.Entity;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class NewRelationModalController implements Initializable {

    private static String name;
    private static ArrayList<Entity> relatedEntities;
    private static ArrayList<Aggregation> relatedAggregations;
    // TextField that contains the name of the Relation assigned by the user
    @FXML
    private TextField nameTextField;
    // List with the entities available to establish a relation
    @FXML
    private VBox toRelateList;
    // Button to cancel the creation of the current Relation
    @FXML
    private Button cancelButton;
    // Button to confirm the attributes of the Entity and create it
    @FXML
    private Button confirmButton;

    public static ArrayList<Entity> getRelatedEntities() {
        return relatedEntities;
    }

    public static ArrayList<Aggregation> getRelatedAggregations() {
        return relatedAggregations;
    }

    public static String getRelationName() {
        return name;
    }

    @FXML
    void cancelButtonPressed(ActionEvent event) {
        closeWindow();
    }

    private void closeWindow() {
        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    @FXML
    void confirmButtonPressed() {
        name = nameTextField.getText();
        ArrayList<Entity> entities = new ArrayList<>();
        ArrayList<Aggregation> aggregations = new ArrayList<>();
        for (Node node : toRelateList.getChildren()) {
            if (((CheckBox) node).isSelected()) { // Add the checked entities to the list
                if (toRelateList.getChildren().indexOf(node) < Main.diagram.getEntities().size()) {
                    entities.add(Main.diagram.getEntities().get(toRelateList.getChildren().indexOf(node)));
                } else {
                    aggregations.add(Main.diagram.getAggregations().get(toRelateList.getChildren().indexOf(node) % Main.diagram.getEntities().size()));
                }
            }
        }
        if ((entities.isEmpty() && aggregations.isEmpty()) || (entities.size() + aggregations.size() > 2)) { // Check if there are between 1 and 2 entities
            Alert alert = new Alert(Alert.AlertType.WARNING, "A relation must have 1 or 2 entities/aggregations", ButtonType.OK);
            alert.setHeaderText("Warning");
            alert.showAndWait();
        } else {
            MainWindowController.setAskedToDrawRelation(true);
            relatedEntities = entities;
            relatedAggregations = aggregations;
            closeWindow();
        }
    }

    @FXML
    void keyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case ENTER:
                confirmButtonPressed();
                break;
            case ESCAPE:
                closeWindow();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (Entity entity : Main.diagram.getEntities()) {
            this.toRelateList.getChildren().add(new CheckBox(entity.getName()));
        }
        for (Aggregation aggregation : Main.diagram.getAggregations()) {
            this.toRelateList.getChildren().add(new CheckBox(aggregation.getName()));
        }
    }
}
