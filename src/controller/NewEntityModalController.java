package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import utilities.SemanticVerifier;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * NewEntityModalController is a class that controls the actions of the
 * NewEntityModal.fxml
 *
 * @author Edward Becerra
 * @author Carlos Rios
 * @author Jose Villar
 * @version 12.09.18 (Modified by: Carlos Rios)
 */
public class NewEntityModalController implements Initializable {

    // TextField that cointains the name of the Entity assigned by the user
    @FXML
    private TextField nameTextField;

    // Button to cancel the creation of the current Entity
    @FXML
    private Button cancelButton;

    // Button to confirm the attributes of the Entity and create it
    @FXML
    private Button confirmButton;

    @FXML
    private static String name;

    public static String getEntityName() {
        return name;
    }

    /**
     * Close the current modal and cancel the current operation
     * @param event
     */
    @FXML
    void cancelButtonPressed(ActionEvent event) {
        closeWindow();
    }

    /**
     * Get all the information needed to create the Entity and create it
     */
    @FXML
    void confirmButtonPressed() {
        if(SemanticVerifier.entityNameHasBeenTaken(nameTextField.getText())){
            Alert alert = new Alert(Alert.AlertType.WARNING, "This name has already been taken", ButtonType.OK);
            alert.showAndWait();
        }else {
            name = nameTextField.getText();
            MainWindowController.setAskedToDrawEntity(true);
            Stage stage = (Stage) confirmButton.getScene().getWindow();
            stage.close();
        }
    }

    @FXML
    void keyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case ENTER:
                confirmButtonPressed();
                break;
            case ESCAPE:
                closeWindow();
        }
    }

    private void closeWindow() {
        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

}
